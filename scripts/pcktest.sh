#!/bin/bash
#
# Update test installation from live backup
# Pass some string as an argument to update and sanitise data
#
# Run as ubuntu user


CURRENTDIR=`dirname $0`
source $CURRENTDIR/config.sh || exit

# sync images
sudo -u www-data rsync -rtup --links --delete --info=progress2 --exclude 'custom' --exclude graphics $PCKDIR/uploads/ $TESTDIR/uploads/

# block search engines
sudo cp /data/phpeace/disallow.txt $TESTDIR/pub/robots.txt
# update logo
sudo cp $PCKUIDIR/test/custom/peacetest.gif $TESTDIR/uploads/graphics/orig/1.gif

# sync db
if [ ! -z "$1" ]; then
    echo "sanitizing database..."
    pcksan.sh
fi

# set perms
sudo chown -R www-data.www-data $TESTDIR

# slack
