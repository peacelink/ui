
UPDATE global SET 
    pub_web='http://dev.peacelink.it:8070',
    admin_web='http://admin.dev.peacelink.it:8070',
    install_key='b8f6cc18d38b5de1ab157ff6024eb4b5',
    title='PhPeace Dev';

UPDATE url_friendly SET url=REPLACE(url,'https://www.peacelink.it','http://dev.peacelink:8070');
UPDATE url_friendly SET url=REPLACE(url,'https://test.peacelink.it','http://admin.dev.peacelink.it:8070');

# Disable non-PeaceLink users
UPDATE users SET 
    active=0,
    login=CONCAT('user-',id_user),
    password='********',
    name=LCASE(LEFT(TO_BASE64(SHA(rand())),8)),
    email=CONCAT('user-',id_user,'@farlocco.it'),
    phone='',
    mobile=''
    WHERE email NOT LIKE '%@peacelink%';

UPDATE people SET 
    active=0,
    contact=0,
    password='********',
    name1=LCASE(LEFT(TO_BASE64(SHA(rand())),8)),
    name2=CONCAT('guest-',id_p),
    email=CONCAT('guest-',id_p,'@farlocco.it'),
    address='',
    phone='',
    token=''
    WHERE email NOT LIKE '%@peacelink%';

UPDATE schedules SET active=0 WHERE id_action>0;
    
UPDATE topics SET domain='',newsletter=0;

TRUNCATE users_log;
TRUNCATE publish_log;
TRUNCATE mailjob_recipients;
TRUNCATE queue;

UPDATE global SET map_path='map', gallery_path='galleries', events_path='events', search_path='search', lists_path='lists', books_path='books', campaign_path='campaign', poll_path='poll', forum_path='forum', org_path='orgs', quotes_path='quotes', users_path='users', media_path='media' WHERE id_install=1;

UPDATE twitters SET active=0,oauth_token_secret='';
