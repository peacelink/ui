<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="root.xsl" />
<xsl:include href="tools.xsl" />


<!-- ###############################
     TOPIC LATEST
     ############################### -->
<xsl:template name="topicLatest">
  <div id="topic-latest" class="pckbox">
    <xsl:choose>
      <xsl:when test="/root/publish/@live='1'">
        <xsl:if test="/root/c_features/feature[@id='32']/items">
          <xsl:apply-templates select="/root/c_features/feature[@id='32']"/>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <script type="text/javascript">
  $(function() {
    htmlLoad('topic-latest','/js/feature.php?id=32&amp;transform',true)
  });
        </script>
      </xsl:otherwise>
    </xsl:choose>
  </div>
</xsl:template>


<!-- ###############################
     ARTICLE FOOTER
     ############################### -->
<xsl:template name="articleFooter">
  <xsl:param name="a"/>
  <!-- Events FFF -->
  <xsl:if test="/root/features/feature[@id='204']/items">
    <div class="pckbox" id="fff-events">
      <h3 class="feature"><xsl:value-of select="/root/features/feature[@id='204']/@name"/></h3>
      <ul class="items">
        <xsl:apply-templates select="/root/features/feature[@id='204']/items/item" mode="mainlist"/>
      </ul>
    </div>
  </xsl:if>
  <!-- Correlated articles -->
  <xsl:if test="/root/features/feature[@id='33']/items">
    <div class="pckbox" id="similar">
      <h3 class="feature"><xsl:value-of select="/root/features/feature[@id='33']/@name"/></h3>
      <ul class="items">
        <xsl:apply-templates select="/root/features/feature[@id='33']/items/item" mode="seclist"/>
      </ul>
    </div>
  </xsl:if>
</xsl:template>


</xsl:stylesheet>
