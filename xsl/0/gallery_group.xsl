<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="common.xsl" />

<xsl:output method="html" encoding="UTF-8" indent="no" />

<xsl:variable name="current_page_title">
<xsl:value-of select="/root/tree/@name"/>
<xsl:if test="/root/gallery/@name"> - <xsl:value-of select="/root/gallery/@name"/></xsl:if>
</xsl:variable>

<!-- ###############################
     CONTENT
     ############################### -->
<xsl:template name="content">
  <xsl:if test="/root/group/@id">
    <xsl:call-template name="breadcrumb"/>
  </xsl:if>
  <xsl:choose>
    <xsl:when test="$subtype='group'">
      <xsl:if test="/root/group/@description!=''">
        <div class="description"><xsl:value-of select="/root/group/@description"/></div>
      </xsl:if>
      <xsl:call-template name="galleries">
        <xsl:with-param name="node" select="/root/group/galleries"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="$subtype='gallery'">
      <xsl:apply-templates mode="summary" select="/root/gallery"/>
      <xsl:call-template name="items">
        <xsl:with-param name="root" select="/root/gallery/images"/>
        <xsl:with-param name="node" select="/root/gallery"/>
      </xsl:call-template>
      <xsl:call-template name="licence">
        <xsl:with-param name="i" select="/root/gallery"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="$subtype='slideshow'">
      <xsl:call-template name="gallerySlideshow">
        <xsl:with-param name="node" select="/root/gallery"/>
      </xsl:call-template>
      <xsl:call-template name="licence">
        <xsl:with-param name="i" select="/root/gallery"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="$subtype='image'">
      <xsl:call-template name="galleryImageItem">
        <xsl:with-param name="i" select="/root/gallery/image"/>
      </xsl:call-template>
    </xsl:when>
  </xsl:choose>
</xsl:template>


<!-- ###############################
     GALLERY [summary]
     ############################### -->
<xsl:template match="gallery" mode="summary">
  <div id="gallery">
    <h2 class="gallery">
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="."/>
        <xsl:with-param name="condition" select="$subtype!='gallery'"/>
      </xsl:call-template>
    </h2>
    <xsl:if test="description!=''">
      <div class="description"><xsl:value-of select="description" disable-output-escaping="yes"/></div>
    </xsl:if>
    <xsl:if test="@date!='' ">
      <div class="gallery-date"><xsl:value-of select="concat(/root/labels/label[@word='date']/@tr,': ',@date)"/></div>
    </xsl:if>
    <xsl:if test="@author!='' ">
      <div class="gallery-author"><xsl:value-of select="concat(/root/labels/label[@word='author']/@tr,': ',@author)"/></div>
    </xsl:if>
    <h3 class="slideshowlink">
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="slideshow"/>
        <xsl:with-param name="name" select="slideshow/@label"/>
      </xsl:call-template>
    </h3>
    <xsl:call-template name="licenceInfo">
      <xsl:with-param name="i" select="."/>
    </xsl:call-template>
  </div>
</xsl:template>


<!-- ###############################
     GALLERIES
     ############################### -->
<xsl:template name="galleries">
  <xsl:param name="node"/>
  <ul class="galleries items">
    <xsl:for-each select="$node/gallery">
      <li>
        <xsl:attribute name="class"><xsl:value-of select="@type"/>-item<xsl:if test="position()=last()"><xsl:text> last</xsl:text></xsl:if></xsl:attribute>
        <xsl:call-template name="galleryItem">
          <xsl:with-param name="i" select="."/>
          <xsl:with-param name="with_details" select="true()"/>
          <xsl:with-param name="fixed_width" select="true()"/>
        </xsl:call-template>
      </li>
    </xsl:for-each>
  </ul>
</xsl:template>


<!-- ###############################
     LEFT BAR
     ############################### -->
<xsl:template name="leftBar">
  <input class="icon" type="checkbox" id="menu-btn" />
  <h2 class="gallery-group">
    <xsl:call-template name="createLink">
      <xsl:with-param name="node" select="/root/tree"/>
      <xsl:with-param name="class" select="'icon'"/>
    </xsl:call-template>
  </h2>
  <xsl:apply-templates select="/root/tree/groups"/>
  <xsl:call-template name="leftBottom"/>
</xsl:template>


<!-- ###############################
     PAGE TITLE
     ############################### -->
<xsl:template name="pageTitle">
<xsl:if test="$preview=true()">[<xsl:value-of select="/root/labels/label[@word='preview']/@tr"/>] -  </xsl:if><xsl:value-of select="/root/tree/@name"/><xsl:if test="/root/gallery/@name"> - <xsl:value-of select="/root/gallery/@name"/></xsl:if>
</xsl:template>


<!-- ###############################
      MENU ITEM
      ############################### -->
<xsl:template mode="groupitem" match="group">
  <xsl:param name="level"/>
  <li>
    <xsl:attribute name="class">level<xsl:value-of select="$level"/><xsl:if test="@id = /root/group/@id or @id=/root/gallery/@id_group"><xsl:text> </xsl:text>selected</xsl:if></xsl:attribute>
    <xsl:call-template name="createLink">
      <xsl:with-param name="node" select="."/>
      <xsl:with-param name="name" select="@name"/>
    </xsl:call-template>
    <xsl:if test="groups">
      <xsl:apply-templates select="groups">
        <xsl:with-param name="level" select="$level + 1"/>
      </xsl:apply-templates>
    </xsl:if>
  </li>
</xsl:template>


<!-- ###############################
      MENU ITEMS
      ############################### -->
<xsl:template match="groups">
  <xsl:param name="level" select="'1'"/>
  <ul class="menu">
    <xsl:apply-templates mode="groupitem">
      <xsl:with-param name="level" select="$level"/>
    </xsl:apply-templates>
  </ul>
</xsl:template>


<!-- ###############################
     GALLERY SLIDESHOW
     ############################### -->
<xsl:template name="gallerySlideshow">
  <xsl:param name="node"/>
  <ul class="slideshow">
    <xsl:for-each select="$node/images/item">
      <li>
        <xsl:call-template name="imageItem">
          <xsl:with-param name="id" select="@id"/>
          <xsl:with-param name="width" select="$gallery_image_width"/>
          <xsl:with-param name="format" select="@format"/>
          <xsl:with-param name="caption" select="@caption"/>
        </xsl:call-template>
        <div class="caption"><xsl:value-of select="@caption"/></div>
      </li>
    </xsl:for-each>
  </ul>
</xsl:template>



</xsl:stylesheet>
