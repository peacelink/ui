<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="show_widgets" select="false()"/>

<!-- ###############################
     ROOT
     ############################### -->
<xsl:template name="root">
  <html>
    <xsl:attribute name="lang">
      <xsl:choose>
        <xsl:when test="/root/topic"><xsl:value-of select="/root/topic/@lang"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="/root/site/@lang"/></xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <head>
      <xsl:call-template name="head"/>
      <meta name="format-detection" content="telephone=no"/>
      <meta name="geo.placename" content="Taranto, IT"/>
      <meta name="geo.country" content="it"/>
      <meta name="dc.language" content="it"/>
      <meta name="application-name" content="PeaceLink"/>
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:site" content="@peacelink" />
      <xsl:call-template name="structuredData"/>
      <xsl:if test="$pagetype='homepage'">
        <link rel="alternate" hreflang="en" href="/en/" />
        <link rel="alternate" hreflang="es" href="/es/" />
        <link rel="alternate" hreflang="fr" href="/fr/" />
      </xsl:if>
      <xsl:if test="/root/topic[@id_group='14'] and $pagetype='topic_home'">
        <link rel="alternate" hreflang="it" href="/" />
      </xsl:if>
      <link rel="apple-touch-icon" sizes="180x180" href="/icon/apple-touch-icon.png"/>
      <link rel="icon" type="image/png" sizes="32x32" href="/icon/favicon-32x32.png"/>
      <link rel="icon" type="image/png" sizes="16x16" href="/icon/favicon-16x16.png"/>
      <link rel="manifest" href="/icon/site.webmanifest"/>
      <link rel="mask-icon" href="/icon/safari-pinned-tab.svg" color="#ff8210"/>
      <link rel="shortcut icon" href="/icon/favicon.ico"/>
      <meta name="msapplication-TileColor" content="#ff8210"/>
      <meta name="msapplication-config" content="/icon/browserconfig.xml"/>
      <meta name="theme-color" content="#ff8210"/>
      <xsl:call-template name="matomoAnalytics">
        <xsl:with-param name="matomo_id" select="'3'" />
        <xsl:with-param name="cookie_domain" select="'peacelink.it'" />
      </xsl:call-template>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"/>
    </head>
    <body class="{/root/publish/@type} subtype-{$subtype}" id="id{/root/publish/@id}">
      <xsl:if test="/root/preview"><xsl:call-template name="previewToolbar"/></xsl:if>
      <div id="main-wrap" >
        <xsl:if test="$subtype='landing'">
          <xsl:attribute name="class"><xsl:value-of select="/root/publish/@landing"/></xsl:attribute>
        </xsl:if>
        <xsl:if test="$subtype='keyword'">
          <xsl:attribute name="class">k-<xsl:value-of select="/root/keyword/@name"/></xsl:attribute>
        </xsl:if>
        <div id="top-nav"><xsl:call-template name="topNavPck"/></div>
        <xsl:choose>
          <xsl:when test="/root/topic and $pagetype!='events'">
            <div id="main">
              <xsl:attribute name="class">
                <xsl:call-template name="mapGroups">
                  <xsl:with-param name="string" select="'group'"/>
                  <xsl:with-param name="id" select="/root/topic/@id_group"/>
                </xsl:call-template>
              </xsl:attribute>
              <div id="left-bar"><xsl:call-template name="leftBarPck" /></div>
              <div id="content"><xsl:call-template name="content" /></div>
              <xsl:if test="/root/topic/@id_group='6'">
                <xsl:call-template name="pckTopics"/>
              </xsl:if>
              <div id="fotonotizia" class="pckbox">
                <xsl:call-template name="fotonotiziaTopic"/>
              </div>
              <div id="right-bar"><xsl:call-template name="rightBarPck" /></div>
            </div>
          </xsl:when>
          <xsl:otherwise>
            <div id="main-global">
              <xsl:choose>
                <xsl:when test="$subtype='landing' and /root/publish/@landing='ariataranto'">
                  <div class="aria-titles">
                    <h1>Aria Taranto - Piattaforma per il monitoraggio dell'inquinamento ambientale</h1>
                  </div>
                  <iframe width="100%" height="100%" src="https://omniscope.app/Air+Pollution/Italy/Taranto.iox/r/Aria+Taranto/" allowfullscreen="true"></iframe>
                </xsl:when>
                <xsl:when test="$subtype='landing' and /root/publish/@landing='ariasiracusa'">
                  <div class="aria-titles">
                    <h1>Aria Siracusa - Piattaforma per il monitoraggio dell'inquinamento ambientale</h1>
                  </div>
                  <iframe width="100%" height="100%" src="https://datastudio.google.com/embed/reporting/5aea75c3-ba80-4a1b-890d-a632ef87ed20/page/MPF7B" frameborder="0" style="border:0" allowfullscreen="true"></iframe>
                </xsl:when>
                <xsl:when test="$subtype='landing' and /root/publish/@landing='video'">
                  <h1 class="video">Canale Youtube di PeaceLink</h1>
                  <script src="https://apis.google.com/js/platform.js"></script>
                  <div class="g-ytsubscribe" data-channel="peacelinkvideo" data-layout="full" data-count="default"></div>
                  <ul id="youtube-videos"></ul>
<script>

var players = [];

// lazy load youtube iframe api
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function onYouTubeIframeAPIReady() {

  $.get(
    "https://www.googleapis.com/youtube/v3/search",{
      part : 'snippet', 
      channelId : 'UC3WwkfbNXH-TxnVFpwSAtSQ',
      type : 'video',
      order: 'date',
      maxResults: 8,
      key: '<xsl:value-of select="/root/site/@google_api_key"/>'},
      function(data) {
        $.each( data.items, function( i, item ) {
          video = youtubeVideo(i, item);
          $('#youtube-videos').append(video);
          var player = new YT.Player('youtube-video'+(i), {
            videoId: item.id.videoId,
            width: '560',
            height: '314',
            playerVars: { 
              'enablejsapi': 1,
              'origin': '<xsl:value-of select="/root/site/@admin"/>'
            },
            events: {
              'onStateChange': onPlayerStateChange
            },
          });
          players.push(player);
        })
      }
  );
}

function onPlayerStateChange(event) {
  if (event.data == YT.PlayerState.PLAYING) {
    for (var i = 0; i &lt; players.length; i++) {
      if (i +1 != event.target.id) {
        pl = players[i];
        pl.stopVideo();
      }
    }
  }
}
</script>
                </xsl:when>
                <xsl:when test="$subtype='keyword'">
                  <div id="content"><xsl:call-template name="landing"/></div>
                  <div id="right-bar"><xsl:call-template name="rightBarPckLanding"/></div>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:if test="$pagetype!='events' and $pagetype!='search' and $pagetype!='gallery_group' and $subtype!='unsub'">
                    <div id="fotonotizia" class="pckbox">
                      <xsl:call-template name="fotonotizia"/>
                    </div>
                  </xsl:if>
                  <xsl:if test="$pagetype='gallery_group'">
                    <div id="left-bar"><xsl:call-template name="leftBarPck" /></div>
                  </xsl:if>
                  <div id="content"><xsl:call-template name="content" /></div>
                  <xsl:if test="$pagetype='gallery_group'">
                    <div id="fotonotizia" class="pckbox">
                      <xsl:call-template name="fotonotiziaTopic"/>
                    </div>
                  </xsl:if>
                  <xsl:choose>
                    <xsl:when test="$pagetype='homepage' and $subtype='unsub'">
                    </xsl:when>
                    <xsl:when test="$pagetype='events'">
                      <div id="right-bar">
                        <xsl:call-template name="eventsInfo"/>
                        <div id="fotonotizia" class="pckbox">
                          <xsl:choose>
                            <xsl:when test="/root/topic">
                              <xsl:call-template name="fotonotiziaTopic"/>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:call-template name="fotonotizia"/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </div>
                        <div class="m-hidden">
                          <xsl:call-template name="siteLatest"/>
                        </div>
                      </div>
                    </xsl:when>
                    <xsl:otherwise>
                      <div id="right-bar"><xsl:call-template name="rightBarPck" /></div>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
            </div>
          </xsl:otherwise>
        </xsl:choose>
        <div id="bottom-bar"><xsl:call-template name="bottomBarPck" /></div>
      </div>
    </body>
  </html>
</xsl:template>


<!-- ###############################
     TOP NAV PCK
     ############################### -->
<xsl:template name="topNavPck">
  <a id="logo">
    <xsl:attribute name="href">
      <xsl:call-template name="createLinkUrl">
        <xsl:with-param name="node" select="/root/site"/>
      </xsl:call-template>
    </xsl:attribute>
  </a>

  <ul id="pck-links">
    <li id="pck-about" class="first">
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="/root/c_features/feature[@id='8']/items/topic_full/topic"/>
        <xsl:with-param name="name" select="'Chi siamo'"/>
      </xsl:call-template>
    </li>
    <li id="pck-contact">
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="/root/c_features/feature[@id='8']/items/topic_full/menu/subtopics//subtopic[@id='911']"/>
        <xsl:with-param name="name" select="'Contattaci'"/>
      </xsl:call-template>
    </li>
    <li id="pck-help">
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="/root/c_features/feature[@id='8']/items/topic_full/menu/subtopics//subtopic[@id='1162']"/>
        <xsl:with-param name="name" select="'Partecipa'"/>
      </xsl:call-template>
    </li>
    <li id="pck-search-mobile">
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="/root//site/search"/>
        <xsl:with-param name="name" select="'Cerca'"/>
      </xsl:call-template>
    </li>
    <li id="pck-support" class="last">
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="/root/c_features/feature[@id='8']/items/topic_full/menu/subtopics//subtopic[@id='2817']"/>
        <xsl:with-param name="name" select="'Sostienici'"/>
      </xsl:call-template>
    </li>
  </ul>
  <xsl:call-template name="bannerGroup"><xsl:with-param name="id" select="'36'"/></xsl:call-template>
  <ul id="content-links">
    <xsl:for-each select="/root/c_features/feature[@id=3]/items/item[@id!=6 and @id!=14]">
      <li>
        <xsl:attribute name="id">
          <xsl:call-template name="mapGroups">
            <xsl:with-param name="string" select="'topic-group'"/>
            <xsl:with-param name="id" select="@id"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:if test="(/root/topic/@id_group=@id or /root/topics/group/@id=@id)"><xsl:attribute name="class">selected</xsl:attribute></xsl:if>
        <xsl:call-template name="createLink">
          <xsl:with-param name="node" select="."/>
        </xsl:call-template>
      </li>
    </xsl:for-each>
    <li id="pck-events">
      <xsl:if test="$pagetype='events' and not(/root/topic)"><xsl:attribute name="class">selected</xsl:attribute></xsl:if>
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="/root/site/events"/>
        <xsl:with-param name="name" select="/root/site/events/@label"/>
      </xsl:call-template>
    </li>
    <li id="pck-lists">
      <a href="https://lists.peacelink.it" title="Mailing Lists di PeaceLink">Liste</a>
    </li>
    <li id="pck-video">
      <a href="/video" title="Ultimi video di PeaceLink">Video</a>
    </li>
    <li id="pck-en">
      <a hreflang="en" title="English site">
        <xsl:attribute name="href">
          <xsl:call-template name="createLinkUrl">
            <xsl:with-param name="node" select="/root/c_features/feature[@id='205']/items/topic_full/topic"/>
          </xsl:call-template>
        </xsl:attribute>English
      </a>
    </li>
    <li id="pck-search" class="search">
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="/root/site/search"/>
      </xsl:call-template>
    </li>
    <xsl:call-template name="userInfo"/>
  </ul>
</xsl:template>


<!-- ###############################
     LEFT BAR PCK
     ############################### -->
<xsl:template name="leftBarPck">
  <xsl:choose>
    <xsl:when test="/root/topic">
      <xsl:choose>
        <xsl:when test="$pagetype='orgs' ">
          <xsl:call-template name="leftBarOrgs"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="navigationMenu"/>
          <div class="m-hidden">
            <xsl:call-template name="leftBottom"/>
            <xsl:if test="/root/topic/lists/list">
              <xsl:call-template name="pckList">
                <xsl:with-param name="url" select="/root/topic/lists/list/@feed"/>
                <xsl:with-param name="archive" select="/root/topic/lists/list/@archive"/>
                <xsl:with-param name="name" select="concat('Lista ',/root/topic/lists/list/@name)"/>
                <xsl:with-param name="id_list" select="/root/topic/lists/list/@id"/>
                <xsl:with-param name="id_topic" select="/root/topic/@id"/>
              </xsl:call-template>
            </xsl:if>
          </div>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="$pagetype='gallery_group' ">
      <xsl:call-template name="leftBar"/>
    </xsl:when>
  </xsl:choose>
  <div class="m-hidden">
    <xsl:call-template name="siteLatest"/>
  </div>
</xsl:template>


<!-- ###############################
     RIGHT BAR PCK
     ############################### -->
<xsl:template name="rightBarPck">
  <xsl:choose>
    <xsl:when test="/root/topic and $pagetype!='events'">
      <xsl:call-template name="nextEventsPck"/>
      <xsl:call-template name="socialeNetwork"/>
    </xsl:when>
    <xsl:otherwise>
      <!-- RIGHT BAR generica -->
      <xsl:if test="$pagetype='map' and /root/topics/group/@id=6">
        <xsl:call-template name="pckYoutube"/>
      </xsl:if>
      <xsl:if test="$pagetype!='error404'">
        <xsl:call-template name="socialeNetwork"/>
        <xsl:call-template name="nextEventsPck">
          <xsl:with-param name="showImage" select="false()"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="$pagetype='homepage'">
        <xsl:call-template name="pckDossier"/>
      </xsl:if>
      <xsl:call-template name="pckList">
        <xsl:with-param name="url" select="'https://lists.peacelink.it/feed/news/news.rss'"/>
        <xsl:with-param name="archive" select="'https://lists.peacelink.it/news/'"/>
        <xsl:with-param name="name" select="'PeaceLink News'"/>
        <xsl:with-param name="id_list" select="'15'"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
  <div class="m-show">
    <xsl:call-template name="siteLatest">
      <xsl:with-param name="load" select="false()"/>
    </xsl:call-template>
  </div>
  <xsl:if test="$pagetype!='events'">
    <xsl:call-template name="randomQuote"/>
  </xsl:if>
</xsl:template>


<!-- ###############################
     SOCIALE.NETWORK
     ############################### -->
<xsl:template name="socialeNetwork">
  <div id="socialenetwork" class="pckbox">
  <h3><a href="https://sociale.network/">Sociale.network</a></h3>
  </div>
</xsl:template>


<!-- ###############################
     TWITTER
     ############################### -->
<xsl:template name="pckTwitter">
  <div id="pck-twitter"><a class="twitter-timeline" data-lang="it" data-height="400" data-theme="light" data-chrome="noheader nofooter noborders" href="https://twitter.com/peacelink?ref_src=twsrc%5Etfw">Tweets by PeaceLink</a> <script async="true" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
</xsl:template>


<!-- ###############################
     FACEBOOK
     ############################### -->
<xsl:template name="pckFacebook">
  <xsl:param name="width" select="'500'"/>
  <div id="pck-facebook" class="pckbox">
    <div class="fb-page" data-href="https://www.facebook.com/retepeacelink/" data-tabs="timeline" data-adapt-container-width="true" data-height="500" data-width="{$width}" data-small-header="true" data-show-facepile="false">
    <xsl:if test="$width='500'">
      <xsl:attribute name="data-show-facepile">true</xsl:attribute>
    </xsl:if>
      <blockquote cite="https://www.facebook.com/retepeacelink/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/retepeacelink/">Pagina Facebook di PeaceLink in caricamento...</a></blockquote>
    </div>
  </div>
</xsl:template>


<!-- ###############################
     NEXT EVENTS PCK
     ############################### -->
<xsl:template name="nextEventsPck">
  <xsl:param name="showImage" select="true()"/>
  <xsl:param name="class" select="'pckbox'"/>
  <div id="next-events" class="{$class}">
    <xsl:choose>
      <xsl:when test="/root/publish/@live='1'">
        <xsl:call-template name="nextEventsRender">
          <xsl:with-param name="node" select="/root/c_features/feature[@id='198']"/>
          <xsl:with-param name="showImage" select="$showImage"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <script type="text/javascript">
$(function() {
  htmlLoad('next-events','/js/feature.php?id=198&amp;transform',true,false,false)
});
        </script>
      </xsl:otherwise>
    </xsl:choose>
  </div>
</xsl:template>


<!-- ###############################
     NEXT EVENTS RENDER
     ############################### -->
<xsl:template name="nextEventsRender">
  <xsl:param name="node"/>
  <xsl:param name="showImage" select="true()"/>
  <xsl:param name="imageWidth" select="$event_thumb_width"/>
  <xsl:variable name="limitEvents" select="'6'"/>
  <h3>
    <xsl:call-template name="createLink">
      <xsl:with-param name="node" select="/root/site/events"/>
      <xsl:with-param name="name" select="$node/@name"/>
      <xsl:with-param name="class" select="'icon'"/>
    </xsl:call-template>
  </h3>
  <ul class="items events">
    <xsl:for-each select="$node/items/item">
      <xsl:if test="not(position() > $limitEvents)">
        <li class="event-item">
          <xsl:call-template name="eventItem">
            <xsl:with-param name="e" select="."/>
            <xsl:with-param name="showImage" select="$showImage"/>
            <xsl:with-param name="imageWidth" select="$imageWidth"/>
          </xsl:call-template>
        </li>
      </xsl:if>
    </xsl:for-each>
  </ul>
</xsl:template>


<!-- ###############################
     FOTONOTIZIA
     ############################### -->
<xsl:template name="fotonotizia">
  <xsl:param name="i" select="/root/c_features/feature[@id='10']/items/item"/>
  <xsl:variable name="src">
    <xsl:call-template name="createLinkUrl">
      <xsl:with-param name="node" select="$i/src"/>
      <xsl:with-param name="cdn" select="/root/site/@cdn!=''"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="link">
    <xsl:choose>
      <xsl:when test="$i/@link!=''"><xsl:value-of select="$i/@link"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="/root/site/@url"/></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <a href="{$link}">
    <img alt="{$i/@caption}" src="{$src}" />
  </a>
  <div class="description">
    <a href="{$link}"><xsl:value-of select="$i/@caption"/></a>
  </div>
</xsl:template>


<!-- ###############################
     FOTONOTIZIA TOPIC
     ############################### -->
<xsl:template name="fotonotiziaTopic">
  <div id="fotonotizia" class="pckbox">
    <script type="text/javascript">
  $(function() {
    htmlLoad('fotonotizia','/js/feature.php?id=10&amp;transform',true)
  });
    </script>
  </div>
</xsl:template>


<!-- ###############################
     mapDocFormat
     ############################### -->
<xsl:template name="mapDocFormat">
  <xsl:param name="format"/>
  <xsl:variable name="ext">
    <xsl:choose>
      <xsl:when test="$format='pdf'">pdf</xsl:when>
      <xsl:otherwise>doc</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:value-of select="concat('format-',$ext)"/>
</xsl:template>


<!-- ###############################
     mapGroups
     ############################### -->
<xsl:template name="mapGroups">
  <xsl:param name="string"/>
  <xsl:param name="id"/>
  <xsl:param name="group">
    <xsl:choose>
      <xsl:when test="$id='1'">pace</xsl:when>
      <xsl:when test="$id='3'">cult</xsl:when>
      <xsl:when test="$id='11'">eco</xsl:when>
      <xsl:when test="$id='12'">sol</xsl:when>
      <xsl:when test="$id='13'">citt</xsl:when>
      <xsl:when test="$id='6'">pck</xsl:when>
      <xsl:when test="$id='2'">osp</xsl:when>
    </xsl:choose>
  </xsl:param>
  <xsl:value-of select="concat($string,'-',$group)"/>
</xsl:template>


<!-- ###############################
     PCK LIST RSS TICKER
     ############################### -->
<xsl:template name="pckList">
  <xsl:param name="url"/>
  <xsl:param name="archive"/>
  <xsl:param name="name"/>
  <xsl:param name="id_list"/>
  <xsl:param name="id_topic" select="'0'"/>
  <div id="mailing-list" class="pckbox">
    <h3><a href="{$archive}" class="icon"><xsl:value-of select="$name"/></a></h3>
    <form action="{/root/site/@base}/liste/actions.php" method="post" id="list-mini-ops" accept-charset="UTF-8">
      <input type="hidden" name="from" value="list"/>
      <input type="hidden" name="id_list" value="{$id_list}"/>
      <input type="hidden" name="id_topic" value="{$id_topic}"/>
      <input type="text" id="email" name="email" placeholder="email"/>
      <input type="submit" name="action_subscribe" value="Iscrizione"/>
    </form>
    <xsl:if test="$url!=''">
      <h4><a href="{$archive}">Archivio pubblico</a></h4>
      <div id="rss-list"></div>
      <script type="text/javascript">
$(function() {
  rssListLoad('rss-list','<xsl:value-of select="$url"/>','<xsl:value-of select="/root/site/@base"/>')
});
      </script>
    </xsl:if>
  </div>
</xsl:template>


<!-- ###############################
     TOPICS PCK
    ############################### -->
<xsl:template name="pckTopics">
  <div class="pckbox pck-topics">
    <h3>
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="/root/c_features/feature[@id=3]/items/item[@id=6]"/>
      </xsl:call-template>
    </h3>
    <ul class="items">
      <xsl:for-each select="/root/c_features/feature[@id=193]/items/item[@archived=0]">
        <xsl:if test="position()=6">
          <li>
            <a href="http://campania.peacelink.net/" title="PeaceLink Campania">PeaceLink Campania</a>
          </li>
        </xsl:if>
        <li>
          <xsl:if test="@id=/root/topic/@id"><xsl:attribute name="class">selected</xsl:attribute></xsl:if>
          <xsl:call-template name="createLink">
            <xsl:with-param name="node" select="."/>
            <xsl:with-param name="link_title" select="description"/>
          </xsl:call-template>
        </li>
      </xsl:for-each>
      <xsl:for-each select="/root/c_features/feature[@id=206]/items/item[@archived=0]">
        <li>
          <xsl:if test="@id=/root/topic/@id"><xsl:attribute name="class">selected</xsl:attribute></xsl:if>
          <xsl:call-template name="createLink">
            <xsl:with-param name="node" select="."/>
            <xsl:with-param name="link_title" select="description"/>
          </xsl:call-template>
        </li>
      </xsl:for-each>
    </ul>
  </div>
</xsl:template>



<!-- ###############################
     SITE LATEST
     ############################### -->
<xsl:template name="siteLatest">
  <xsl:param name="load" select="true()"/>
  <div class="pckbox site-latest">
    <xsl:choose>
      <xsl:when test="/root/publish/@live='1'">
        <xsl:call-template name="siteLatestRender">
          <xsl:with-param name="node" select="/root/c_features/feature[@id='199']"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$load=true()">
          <script type="text/javascript">
  $(function() {
    htmlLoad('site-latest','/js/feature.php?id=199&amp;transform',true,false,true)
  });
          </script>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </div>
</xsl:template>


<!-- ###############################
     SITE LATEST RENDER
     ############################### -->
<xsl:template name="siteLatestRender">
  <xsl:param name="node"/>
    <h3>Dal sito</h3>
    <ul class="items">
      <xsl:apply-templates select="$node/items/item" mode="latestlist"/>
    </ul>
</xsl:template>


<!-- ###############################
     SEARCH PCK
    ############################### -->
<xsl:template name="searchPck">
<form method="get" id="search-form-pck" accept-charset="{/root/site/@encoding}">
<xsl:attribute name="action">
<xsl:call-template name="createLinkUrl">
<xsl:with-param name="node" select="/root/site/search"/>
</xsl:call-template>
</xsl:attribute>
<fieldset>
<legend><xsl:value-of select="/root/site/search/@label"/></legend>
<xsl:if test="$preview='1'"><input type="hidden" name="id_type" value="18"/></xsl:if>
<xsl:if test="/root/topic">
in <select name="id_topic">
<option value="{/root/topic/@id}"><xsl:value-of select="/root/topic/@name"/></option>
<option value="0">peacelink.it</option>
</select>
</xsl:if>
<input type="text" name="q" value="{/root/search/@q}" class="search-input"/>
<input type="submit" value="{/root/site/search/@name}" class="search-submit"/>
</fieldset>
</form>
</xsl:template>
	  
	  
<!-- ###############################
     DOSSIER PCK
     ############################### -->
<xsl:template name="pckDossier">
  <xsl:param name="f" select="/root/c_features/feature[@id='203']"/>
  <div class="pckbox pck-dossier">
    <h3>
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="$f/info"/>
        <xsl:with-param name="name" select="$f/@description"/>
      </xsl:call-template>
    </h3>
    <ul class="items">
      <xsl:apply-templates select="$f/items/item" mode="mainlist"/>
    </ul>
  </div>
</xsl:template>


<!-- ###############################
     BOTTOM BAR PCK
     ############################### -->
<xsl:template name="bottomBarPck">
  PeaceLink C.P. 2009 - 74100 Taranto (Italy) - CCP 13403746 - Sito realizzato con 
  <a href="https://www.phpeace.org">PhPeace <xsl:value-of select="/root/site/@phpeace"/></a> - 
  <xsl:call-template name="createLink">
    <xsl:with-param name="node" select="/root/c_features/feature[@id='8']/items/topic_full/menu/subtopics//subtopic[@id='2074']"/>
  <xsl:with-param name="name" select="'Informativa sulla Privacy'"/>
  </xsl:call-template>
  - 
  <a href="https://www.peacelink.it/peacelink/a/41776.html">Informativa sui cookies</a> - 
  <a href="https://www.peacelink.it/peacelink/diritto-di-replica">Diritto di replica</a> - 
  <a href="mailto:associazione.peacelink@pec.it" title="Posta Elettronica Certificata">Posta elettronica certificata (PEC)</a>
  <xsl:if test="$preview=false()">
    <script type="text/javascript" src="/cookie-bar/cookiebar-latest.min.js?forceLang=it&amp;tracking=1&amp;thirdparty=1&amp;noGeoIp=1&amp;remember=90&amp;scrolling=1&amp;privacyPage=https%3A%2F%2Fwww.peacelink.it%2Fpeacelink%2Finformativa-estesa-sui-cookies"></script>
  </xsl:if>
</xsl:template>


<!-- ###############################
      PEACELINK YOUTUBE
      ############################### -->
<xsl:template name="pckYoutube">
  <div class="pckbox" id="youtube">
    <h3><a href="https://www.youtube.com/user/peacelinkvideo" title="Canale YouTube di PeaceLink">Canale YouTube di PeaceLink</a></h3>
    <iframe src="https://www.youtube.com/embed/?listType=user_uploads&amp;list=peacelinkvideo" width="400" height="332"></iframe>
  </div>
</xsl:template>


<!-- ###############################
     GALLERIE
     ############################### -->
<xsl:template name="gallerie">
  <div class="pckbox2" id="gallerie">
    <h3 class="feature">
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="/root/site/galleries"/>
        <xsl:with-param name="name" select="'Gallerie Fotografiche'"/>
      </xsl:call-template>
    </h3>
    <xsl:call-template name="bannerGallery">
      <xsl:with-param name="id_group" select="1"/>
    </xsl:call-template>
  </div>
</xsl:template>


<!-- ###############################
     STRUCTURED DATA
     ############################### -->
<xsl:template name="structuredData">
  <script type="application/ld+json">
  <xsl:value-of select="/root/@structured_data"/>
  </script>
</xsl:template>


<!-- PLACEHOLDERS -->

<!-- ###############################
     CSS CUSTOM
     ############################### -->
<xsl:template name="cssCustom">
</xsl:template>


<!-- ###############################
 JAVASCRIPT CUSTOM
 ############################### -->
<xsl:template name="javascriptCustom">
</xsl:template>


<!-- ###############################
     LEFT BOTTOM
     ############################### -->
<xsl:template name="leftBottom">
</xsl:template>


</xsl:stylesheet>
