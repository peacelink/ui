<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="common.xsl" />

<xsl:output method="html" encoding="UTF-8" indent="no" />

<xsl:variable name="title">
  <xsl:choose>
    <xsl:when test="$subtype='landing' and /root/publish/@landing='ariataranto'">Aria Taranto</xsl:when>
    <xsl:when test="$subtype='keyword'"><xsl:value-of select="/root/keyword/@name"/></xsl:when>
    <xsl:otherwise>Telematica per la pace</xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="current_page_title" select="concat(/root/site/@title,' - ',$title)"/>

<xsl:variable name="user_id" select="/root/user/@id"/>


<!-- ###############################
     CONTENT
     ############################### -->
<xsl:template name="content">
  <xsl:call-template name="feedback"/>
  <xsl:choose>
    <xsl:when test="$subtype='unsub'">
      <h3>
      <xsl:call-template name="createLink">
        <xsl:with-param name="node" select="/root/site"/>
        <xsl:with-param name="name" select="'Torna alla homepage'"/>
      </xsl:call-template>
      </h3>
    </xsl:when>
    <xsl:otherwise>
      <xsl:for-each select="/root/features/feature[@id_function='1']">
        <xsl:sort select="items/item/@ts" order="descending"/>
        <div class="pckbox">
          <xsl:attribute name="id">
            <xsl:call-template name="mapGroups">
              <xsl:with-param name="string" select="'box-group'"/>
              <xsl:with-param name="id" select="params/@id_topic_group"/>
            </xsl:call-template>
          </xsl:attribute>
          <h3>
            <xsl:call-template name="createLink">
              <xsl:with-param name="node" select="info"/>
              <xsl:with-param name="name" select="@name"/>
              <xsl:with-param name="class" select="'icon'"/>
            </xsl:call-template>
          </h3>
          <xsl:choose>
            <!-- PCK -->
            <xsl:when test="params/@id_topic_group='6'">
              <ul class="items highlight">
                <li>
                  <xsl:call-template name="articleItem">
                    <xsl:with-param name="a" select="/root/c_features/feature[@id='13']/items/item"/>
                    <xsl:with-param name="show_topic" select="true()"/>
                    <xsl:with-param name="highlight" select="true()"/>
                  </xsl:call-template>
                </li>
              </ul>
              <ul class="items cols-3">
                <xsl:for-each select="items/item[topic/@id!='1']">
                  <xsl:if test="position() &lt;= 2">
                    <li>
                      <xsl:call-template name="articleItem">
                        <xsl:with-param name="a" select="."/>
                        <xsl:with-param name="show_path" select="true()"/>
                      </xsl:call-template>
                    </li>
                  </xsl:if>
                </xsl:for-each>
                <xsl:variable name="a" select="/root/features/feature[@id='202']/items/xml/feature_group/items/item"/>
                <li class="last">
                  <div class="article-item">
                    <div class="breadcrumb icon">
                      <xsl:call-template name="createLink">
                        <xsl:with-param name="node" select="$a/topic"/>
                      </xsl:call-template>
                    </div>
                    <xsl:if test="$a/halftitle!=''">
                      <div class="halftitle"><xsl:value-of select="$a/halftitle" disable-output-escaping="yes"/></div>
                    </xsl:if>
                    <h3>
                      <xsl:call-template name="createLink">
                        <xsl:with-param name="name"><xsl:value-of select="$a/headline" disable-output-escaping="yes"/></xsl:with-param>
                        <xsl:with-param name="node" select="$a"/>
                        <xsl:with-param name="target" select="'_blank'"/>
                      </xsl:call-template>
                    </h3>
                    <div class="subhead"><xsl:value-of select="$a/subhead" disable-output-escaping="yes"/></div>
                    <div class="notes">
                      <xsl:if test="$a/@show_date='1' and $a/@display_date"><xsl:value-of select="$a/@display_date"/></xsl:if>
                      <xsl:if test="$a/@show_author='1' and $a/author/@name!=''">
                        <xsl:if test="$a/@show_date='1' and $a/@display_date"> - </xsl:if>
                        <xsl:value-of select="$a/author/@name"/>
                        <xsl:if test="$a/author/@notes!=''"> (<xsl:value-of select="$a/author/@notes"/>)</xsl:if>
                      </xsl:if>
                    </div>
                  </div>
                </li>
              </ul>
            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="items/item[@highlight='1' and image]">
                  <xsl:variable name="highlight_id"><xsl:value-of select="items/item[@highlight='1' and image]/@id"/></xsl:variable>
                  <ul class="items highlight">
                    <li>
                      <xsl:call-template name="articleItem">
                        <xsl:with-param name="a" select="items/item[@id=$highlight_id]"/>
                        <xsl:with-param name="show_topic" select="true()"/>
                      </xsl:call-template>
                    </li>
                  </ul>
                  <ul class="items cols-3">
                    <xsl:apply-templates select="items/item[@id!=$highlight_id]" mode="fulllist"/>
                  </ul>
                </xsl:when>
                <xsl:otherwise>
                  <ul class="items">
                    <xsl:if test="not(items/item/image)">
                      <xsl:attribute name="class">items cols-2</xsl:attribute>
                    </xsl:if>
                    <xsl:apply-templates select="items/item" mode="fulllist"/>
                  </ul>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </div>
      </xsl:for-each>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- ###############################
     LANDING
     ############################### -->
<xsl:template name="landing">
  <xsl:variable name="k" select="/root/keyword"/>
  <h1><xsl:value-of select="$k/@description"/></h1>
  <div class="content"><xsl:value-of select="$k/content" disable-output-escaping="yes"/></div>
  <!-- articles -->
  <div class="pckbox klanding-articles">
    <h3>Articoli</h3>
    <ul class="items">
      <xsl:apply-templates select="$k/articles/item" mode="fulllist"/>
    </ul>
  </div>
  <!-- links -->
  <xsl:if test="$k/links/item">
    <div class="pckbox klanding-links">
      <h3>Links</h3>
      <ul class="items">
        <xsl:apply-templates select="$k/links/item" mode="fulllist"/>
      </ul>
    </div>
  </xsl:if>
</xsl:template>


<!-- ###############################
     LANDING RIGHT PCK LANDING
     ############################### -->
<xsl:template name="rightBarPckLanding">
  <xsl:variable name="k" select="/root/keyword"/>
  <!-- youtube -->
  <xsl:if test="$k/@youtube != ''">
    <div class="pckbox klanding-youtube" id="youtube">
      <h3><a href="{$k/@youtube}" title="Playlist {$k/@name}">Playlist Youtube</a></h3>
      <iframe src="{$k/@youtube}" width="400" height="332"></iframe>
    </div>
  </xsl:if>
  <!-- events -->
  <xsl:if test="$k/events/item">
    <div id="next-events" class="pckbox klanding-events">
      <xsl:variable name="limitEvents" select="'10'"/>
      <h3>Eventi - <a href="/calendario/insert.php?subtype=insert">Segnala</a></h3>
      <ul class="items events">
        <xsl:for-each select="$k/events/item">
          <xsl:if test="not(position() > $limitEvents)">
            <li class="event-item">
              <xsl:call-template name="eventItem">
                <xsl:with-param name="e" select="."/>
                <xsl:with-param name="showImage" select="true()"/>
                <xsl:with-param name="imageWidth" select="$event_thumb_width"/>
              </xsl:call-template>
            </li>
          </xsl:if>
        </xsl:for-each>
      </ul>
    </div>
  </xsl:if>
  <div id="socialenetwork" class="pckbox snlanding">
    <xsl:choose>
      <xsl:when test="$k/@name='albert'">
        <h3><a href="https://sociale.network/tags/pcknews" target="_blank">Sociale.network #pcknews</a></h3>
      </xsl:when>
      <xsl:otherwise>
        <h3><a href="https://sociale.network/tags/{$k/@name}" target="_blank">Sociale.network #<xsl:value-of select="$k/@name"/></a></h3>
      </xsl:otherwise>
    </xsl:choose>
  </div>
  <xsl:choose>
    <xsl:when test="$k/@name='euromissili'">
      <xsl:call-template name="pckList">
        <xsl:with-param name="url" select="'https://lists.peacelink.it/feed/disarmo/disarmo.rss'"/>
        <xsl:with-param name="archive" select="'https://lists.peacelink.it/disarmo/'"/>
        <xsl:with-param name="name" select="'Lista Disarmo'"/>
        <xsl:with-param name="id_list" select="'1'"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="pckList">
        <xsl:with-param name="url" select="'https://lists.peacelink.it/feed/pace/pace.rss'"/>
        <xsl:with-param name="archive" select="'https://lists.peacelink.it/pace/'"/>
        <xsl:with-param name="name" select="'Lista Pace'"/>
        <xsl:with-param name="id_list" select="'17'"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- ###############################
     PAGE TITLE
     ############################### -->
<xsl:template name="pageTitle">
<xsl:if test="$preview=true()">[<xsl:value-of select="/root/labels/label[@word='preview']/@tr"/>] - </xsl:if><xsl:value-of select="/root/site/@title"/> - <xsl:value-of select="/root/labels/label[@word='homepage']/@tr"/>
</xsl:template>


</xsl:stylesheet>

