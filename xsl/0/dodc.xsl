<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" encoding="UTF-8" indent="no" />

<xsl:include href="common.xsl" />

<xsl:variable name="title">
  <xsl:choose>
    <xsl:when test="$subtype='contractors'">Fornitori italiani</xsl:when>
    <xsl:when test="$subtype='offices'">Uffici committenti</xsl:when>
    <xsl:when test="$subtype='contractor'"><xsl:value-of select="/root/dodc/dodc_contractor/@name"/></xsl:when>
    <xsl:when test="$subtype='office'"><xsl:value-of select="/root/dodc/dodc_office/@name"/></xsl:when>
    <xsl:when test="$subtype='contract'">Contratto <xsl:value-of select="/root/dodc/dodc_contract/@number"/></xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="key('label','homepage')/@tr"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="current_page_title" select="concat(/root/dodc/@label,' - ',$title)"/>


<!-- ###############################
     CONTENT
     ############################### -->
<xsl:template name="content">
  <div class="breadcrumb icon"><xsl:call-template name="dodcBreadcrumb"/></div>
  <xsl:call-template name="feedback"/>
  <div id="dodc-content">
    <xsl:choose>
      <xsl:when test="$subtype='contract'">
        <h3># <xsl:value-of select="/root/dodc/dodc_contract/@number"/></h3>
        <p>Data: <xsl:value-of select="/root/dodc/dodc_contract/@start_date"/> (anno fiscale <xsl:value-of select="/root/dodc/dodc_contract/@fiscal_year"/>)</p>
        <p>Ammontare: <xsl:value-of select="/root/dodc/dodc_contract/@iamount"/></p>
        <p class="description"><xsl:if test="/root/dodc/dodc_contract/@notes!=''"><xsl:value-of select="/root/dodc/dodc_contract/@notes"/></xsl:if></p>
        <h3>Committente</h3>
        <p>
          <xsl:call-template name="createLink">
            <xsl:with-param name="node" select="/root/dodc/dodc_contract/dodc_office"/>
          </xsl:call-template>
          (<xsl:value-of select="/root/dodc/dodc_contract/dodc_office/@agency"/>)
          <div class="description"><xsl:value-of select="/root/dodc/dodc_contract/dodc_office/@description"/></div>
        </p>
        <h3>Fornitore</h3>
        <p>
          <xsl:call-template name="createLink">
            <xsl:with-param name="node" select="/root/dodc/dodc_contract/dodc_contractor"/>
          </xsl:call-template>
        </p>
      </xsl:when>
      <xsl:when test="$subtype='contractor'">
        <h1><xsl:value-of select="/root/dodc/dodc_contractor/@name"/></h1>
        <p><xsl:value-of select="/root/dodc/dodc_contractor/@address"/> - <xsl:value-of select="/root/dodc/dodc_contractor/@city"/></p>
        <p><xsl:if test="/root/dodc/dodc_contractor/@cage!=''">N/CAGE: <xsl:value-of select="/root/dodc/dodc_contractor/@cage"/></xsl:if></p>
        <xsl:call-template name="items">
          <xsl:with-param name="root" select="/root/dodc/dodc_contractor/items"/>
          <xsl:with-param name="node" select="/root/dodc/dodc_contractor"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$subtype='contractors'">
        <xsl:call-template name="items">
          <xsl:with-param name="root" select="/root/dodc/contractors/items"/>
          <xsl:with-param name="node" select="/root/dodc/contractors"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$subtype='office'">
        <h1><xsl:value-of select="/root/dodc/dodc_office/@name"/></h1>
        <p>Agency: <xsl:value-of select="/root/dodc/dodc_office/@agency"/></p>
        <p><xsl:value-of select="/root/dodc/dodc_office/@description"/></p>
        <p><xsl:value-of select="/root/dodc/dodc_office/@counter"/> contratti per un totale di $ <xsl:value-of select="/root/dodc/dodc_office/@total"/></p>
        <xsl:call-template name="items">
          <xsl:with-param name="root" select="/root/dodc/dodc_office/items"/>
          <xsl:with-param name="node" select="/root/dodc/dodc_office"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$subtype='offices'">
        <xsl:call-template name="items">
          <xsl:with-param name="root" select="/root/dodc/offices/items"/>
          <xsl:with-param name="node" select="/root/dodc/offices"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="dodcHome"/>
      </xsl:otherwise>
    </xsl:choose>
  </div>
</xsl:template>


<!-- ###############################
     DODC BREADCRUMB
     ############################### -->
<xsl:template name="dodcBreadcrumb">
  <xsl:call-template name="createLink">
    <xsl:with-param name="node" select="/root/dodc"/>
    <xsl:with-param name="name" select="/root/dodc/@label"/>
  </xsl:call-template>
  <xsl:choose>
    <xsl:when test="/root/modc/search_terms">
      <xsl:value-of select="$breadcrumb_separator"/>
      <xsl:call-template name="createLink">
        <xsl:with-param name="name" select="key('label','search')/@tr"/>
        <xsl:with-param name="node" select="/root/dodc/search"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:if test="$subtype='office'">
        <xsl:value-of select="$breadcrumb_separator"/>
        <xsl:call-template name="createLink">
          <xsl:with-param name="name" select="'Uffici committenti'"/>
          <xsl:with-param name="node" select="/root/dodc/offices"/>
        </xsl:call-template>      
      </xsl:if>
      <xsl:if test="$subtype='contractor'">
        <xsl:value-of select="$breadcrumb_separator"/>
        <xsl:call-template name="createLink">
          <xsl:with-param name="name" select="'Fornitori italiani'"/>
          <xsl:with-param name="node" select="/root/dodc/contractors"/>
        </xsl:call-template>      
      </xsl:if>
      <xsl:if test="$title!='' ">
        <xsl:value-of select="concat($breadcrumb_separator,$title)"/>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- ###############################
     DODC HOME
     ############################### -->
<xsl:template name="dodcHome">
  <h2>Fornitori del Pentagono in Italia</h2>
  <p>Questo database raccoglie i fornitori del Dipartimento della Difesa statunitense in Italia dal 1997 al 2011, estratti dai suoi archivi pubblici.</p>
  <p>Gli archivi dei contratti stipulati dal Pentagono, e dei relativi fornitori, sono pubblici dal 1997 e nel corso del tempo sono stati resi disponibili su Internet in diversi formati e indirizzi. Prima del 2007 erano pubblicati su server che oggi non sono più raggiungibili, dal 2007 è invece possibile effettuare interrogazioni anche avanzate su <a href="https://www.fpds.gov" target="_blank">Federal Procurement Data System - Next Generation</a> e su <a href="http://www.usaspending.gov" target="_blank">USA Spending</a></p>
  <p>Per maggiori informazioni sulla storia e sulla metodologia utilizzata si rimanda all'articolo di Francesco Iannuzzelli <a href="/disarmo/a/47019.html">I fornitori italiani del Pentagono</a>.</p>
  <ul class="disc">
    <li><a href="contractors.php">Elenco fornitori italiani</a></li>
    <li><a href="offices.php">Uffici committenti</a></li>
  </ul>
</xsl:template>


<!-- ###############################
     DODC CONTRACT ITEM
     ############################### -->
<xsl:template name="dodcContractItem">
  <xsl:param name="i"/>
  <a href="contract.php?id={$i/@id_contract}"><xsl:value-of select="$i/@start_date"/> - <xsl:value-of select="$i/@number"/></a>
  <br/><xsl:value-of select="$i/@notes"/>
  <xsl:if test="$subtype='dodc_office'">
    <br/>Fornitore: <xsl:value-of select="$i/@contractor_name"/>
  </xsl:if>
  <span class="total">$ <xsl:value-of select="$i/@iamount"/></span>
</xsl:template>


<!-- ###############################
     DODC CONTRACTOR ITEM
     ############################### -->
<xsl:template name="dodcContractorItem">
  <xsl:param name="i"/>
  <a href="contractors.php?id={$i/@id_contractor}"><xsl:value-of select="$i/@name"/></a> <span class="total">$ <xsl:value-of select="$i/@total"/></span>
</xsl:template>


<!-- ###############################
     DODC OFFICE ITEM
     ############################### -->
<xsl:template name="dodcOfficeItem">
  <xsl:param name="i"/>
  <a href="offices.php?id={$i/@id_office}"><xsl:value-of select="$i/@name"/></a> <span class="total">$ <xsl:value-of select="$i/@total"/></span>
</xsl:template>


</xsl:stylesheet>
