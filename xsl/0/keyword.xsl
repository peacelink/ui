<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="common.xsl" />

<xsl:output method="html" encoding="UTF-8" indent="no" doctype-system="http://www.w3.org/TR/html4/strict.dtd"  doctype-public="-//W3C//DTD HTML 4.01//EN" />

<xsl:include href="common_global.xsl" />

<xsl:variable name="current_page_title">
  <xsl:choose>
    <xsl:when test="/root/keyword/@id_type='4'">
      <xsl:value-of select="/root/keyword/@description"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="/root/keyword/@k"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:variable>	

<!-- ###############################
     CONTENT
     ############################### -->
<xsl:template name="content">
  <xsl:if test="/root/keyword">
    <xsl:if test="/root/keyword/@id_type!='4'">
      <div class="breadcrumb icon">
        Parole chiave
        <xsl:value-of select="$breadcrumb_separator"/>
        <xsl:value-of select="/root/keyword/@k"/>
      </div>
    </xsl:if>
    <div id="keyword">
      <xsl:choose>
        <xsl:when test="/root/keyword/@id_type='4'">
          <h1><xsl:value-of select="/root/keyword/@description"/></h1>
        </xsl:when>
        <xsl:otherwise>
          <h1><xsl:value-of select="/root/keyword/@k"/></h1>
          <xsl:if test="/root/keyword/@description != ''">
            <div class="description"><xsl:value-of select="/root/keyword/@description"/></div>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:call-template name="items">
        <xsl:with-param name="root" select="/root/keyword/articles"/>
        <xsl:with-param name="node" select="/root/keyword"/>
      </xsl:call-template>
    </div>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>

