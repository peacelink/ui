<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" encoding="UTF-8" indent="no" />

<xsl:include href="common.xsl" />

<xsl:variable name="ei" select="/root/events/info"/>

<xsl:variable name="title">
  <xsl:choose>
    <xsl:when test="/root/publish/@subtype='event'"><xsl:value-of select="/root/event/@title"/> - <xsl:value-of select="/root/event/@start_date"/></xsl:when>
    <xsl:when test="/root/publish/@subtype='next'"><xsl:value-of select="key('label','next_events')/@tr"/></xsl:when>
    <xsl:when test="/root/publish/@subtype='month'"><xsl:value-of select="concat(/root/events/info/month/@name,' ',/root/events/info/month/@year)"/></xsl:when>
    <xsl:when test="/root/publish/@subtype='insert'"><xsl:value-of select="key('label','event_submit')/@tr"/></xsl:when>
    <xsl:when test="/root/publish/@subtype='map'">Mappa del calendario di PeaceLink</xsl:when>
    <xsl:when test="/root/publish/@subtype='search'">
      Ricerca eventi<xsl:if test="/root/events/future/@q!=''">: &quot;<xsl:value-of select="/root/events/future/@q"/>&quot;</xsl:if>
    </xsl:when>
    <xsl:when test="/root/publish/@subtype='day'"><xsl:value-of select="concat(key('label','events_of_day')/@tr,' ',/root/events/info/curr_day/@label)"/></xsl:when>
    <xsl:otherwise><xsl:value-of select="key('label','calendar')/@tr"/></xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="current_page_title" select="$title"/>


<!-- ###############################
     CONTENT
     ############################### -->
<xsl:template name="content">
  <xsl:call-template name="feedback"/>
  <xsl:choose>
    <xsl:when test="/root/publish/@subtype='insert'">
      <h1><xsl:value-of select="$title"/></h1>
      <xsl:call-template name="eventInsert"/>
    </xsl:when>
    <xsl:when test="/root/publish/@subtype='search'">
      <div class="breadcrumb icon"><xsl:value-of select="$title"/></div>
      <xsl:call-template name="eventSearch"/>
    </xsl:when>
    <xsl:when test="/root/publish/@subtype='map'">
      <h1><xsl:value-of select="$title"/></h1>
      <p class="notes">Eventi del prossime 4 settimane</p>
      <div id="google-map"></div>
      <script type="text/javascript">
        loadGoogleMap('events');
      </script>
    </xsl:when>
    <xsl:otherwise>
      <div id="calendar"></div>
      <div id="event-content">
        <xsl:if test="/root/publish/@subtype='event'">
          <xsl:call-template name="eventContent"/>
          <script type="text/javascript">
            var loadEvent = true;
          </script>
        </xsl:if>
      </div>
      <xsl:variable name="f" select="/root/c_features/feature[@id='198']"/>
      <xsl:if test="/root/c_features/feature[@id='198']/items">
        <div id="next-events" class="events-grid">
        <h2 class="m-show icon">Prossimi appuntamenti</h2>
          <ul class="items events">
            <xsl:for-each select="$f/items/item">
              <li class="event-item">
                <xsl:call-template name="eventItemFull">
                  <xsl:with-param name="e" select="."/>
                  <xsl:with-param name="showImage" select="true()"/>
                  <xsl:with-param name="imageWidth" select="'234'"/>
                </xsl:call-template>
              </li>
            </xsl:for-each>
          </ul>
        </div>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- ###############################
     DAY EVENTS
     ############################### -->
<xsl:template name="dayEvents">
<ul class="items events">
  <xsl:for-each select="/root/events/event">
    <li>
      <xsl:attribute name="class"><xsl:value-of select="@type"/>-item<xsl:if test="position()=last()"><xsl:text> last</xsl:text></xsl:if></xsl:attribute>
      <xsl:call-template name="eventItem">
        <xsl:with-param name="e" select="."/>
        <xsl:with-param name="showDate" select="false()"/>
        <xsl:with-param name="showImage" select="true()"/>
      </xsl:call-template>
    </li>
  </xsl:for-each>
</ul>
</xsl:template>


<!-- ###############################
     DAY TOP BAR
     ############################### -->
<xsl:template name="dayTopBar">
<ul class="day-topbar">
<li class="dow{$ei/prev_day/@dow}" id="bar-prev-day">
<xsl:call-template name="createLink">
<xsl:with-param name="node" select="$ei/prev_day"/>
<xsl:with-param name="name" select="concat($ei/prev_day/@dow_name,' ',$ei/prev_day/@dom)"/>
<xsl:with-param name="follow" select="false()"/>
</xsl:call-template>
</li>
<li class="dow{$ei/curr_day/@dow}" id="bar-curr-day">
<xsl:call-template name="createLink">
<xsl:with-param name="node" select="$ei/curr_day"/>
<xsl:with-param name="name" select="concat($ei/curr_day/@dow_name,' ',$ei/curr_day/@label)"/>
<xsl:with-param name="condition"><xsl:if test="/root/event">true</xsl:if></xsl:with-param>
</xsl:call-template>
</li>
<li class="dow{$ei/next_day/@dow}" id="bar-next-day">
<xsl:call-template name="createLink">
<xsl:with-param name="node" select="$ei/next_day"/>
<xsl:with-param name="name" select="concat($ei/next_day/@dow_name,' ',$ei/next_day/@dom)"/>
<xsl:with-param name="follow" select="false()"/>
</xsl:call-template>
</li>
</ul>
</xsl:template>


<!-- ###############################
     EVENT CONTENT
     ############################### -->
<xsl:template name="eventContent">
  <xsl:param name="e" select="/root/event"/>
  <xsl:if test="$e/image">
    <xsl:call-template name="imageNode">
      <xsl:with-param name="id" select="$e/image/@id"/>
      <xsl:with-param name="node" select="$e/image"/>
    </xsl:call-template>
  </xsl:if>

  <div class="event-type"><xsl:value-of select="$e/@event_type"/></div>

  <h1 data-start="{$e/@start}"><xsl:value-of select="$e/@title"/></h1>

  <div class="event-time">
    <xsl:value-of select="$e/@start_date"/>
    <xsl:if test="$e/@allday='0'">
      <div>
        <xsl:value-of select="$e/@start_time"/>
        <xsl:if test="$e/@length &gt; 0"> (<xsl:value-of select="key('label','length')/@tr"/>: <xsl:value-of select="$e/@length"/><xsl:text> </xsl:text><xsl:value-of select="key('label','hours')/@tr"/>)</xsl:if>
      </div>
    </xsl:if>
  </div>

  <xsl:if test="$e/@address!=''">
    <div class="event-place icon">
      <a href="{$e/@gmaps}" target="_blank">
        <xsl:value-of select="$e/@address"/>
        <xsl:if test="$e/@address!='' and $e/@place!=''">, </xsl:if>
        <xsl:value-of select="$e/@place"/>
        <xsl:if test="$e/@geo_name"> (<xsl:value-of select="$e/@geo_name"/>)</xsl:if>
      </a>
      <xsl:if test="$e/place_details != ''">
        <div class="description"><xsl:value-of select="$e/place_details" disable-output-escaping="yes"/></div>
      </xsl:if>
    </div>
  </xsl:if>

  <div class="event-desc"><xsl:value-of select="$e/description" disable-output-escaping="yes"/></div>

  <xsl:call-template name="eventContentFooter"/>
</xsl:template>


<!-- ###############################
     EVENT CONTENT ARTICLE
     ############################### -->
<xsl:template name="eventContentArticle">
<xsl:param name="e" select="/root/event"/>
<xsl:if test="$e/article">
<div class="event-article"><xsl:value-of select="key('label','see_event_article')/@tr"/>:
<div><xsl:value-of select="$e/article/topic/@name"/>: 
<xsl:call-template name="createLink">
<xsl:with-param name="node" select="$e/article"/>
<xsl:with-param name="name" select="$e/article/headline"/>
</xsl:call-template></div>
</div>
</xsl:if>
</xsl:template>


<!-- ###############################
     EVENT CONTENT ORG
     ############################### -->
<xsl:template name="eventContentOrg">
<xsl:param name="e" select="/root/event"/>
  <xsl:if test="$e/org">
    <div class="event-org icon">Organizzato dall'associazione 
      &quot;<a href="{$e/org/@url}">
        <xsl:value-of select="$e/org/@name"/>
      </a>&quot;
    </div>
  </xsl:if>
</xsl:template>


<!-- ###############################
     EVENT CONTENT FOOTER
     ############################### -->
<xsl:template name="eventContentFooter">
  <xsl:call-template name="eventContentNotes"/>
  <xsl:call-template name="eventContentArticle"/>
  <xsl:call-template name="eventContentOrg"/>
  <div class="icon add-calendar">Aggiungi a calendario: 
    <a href="{/root/event/@export_google}" target="_blank" rel="nofollow">Google</a> - 
    <a href="{/root/event/@export_webcal}" target="_blank" rel="nofollow">Outlook</a> - 
    <a href="{/root/event/@export_webcal}" target="_blank" rel="nofollow">Apple</a> - 
    <a href="{/root/event/@export}" target="_blank" rel="nofollow">Altri</a>
  </div>
</xsl:template>


<!-- ###############################
     EVENT CONTENT NOTES
     ############################### -->
<xsl:template name="eventContentNotes">
  <xsl:param name="e" select="/root/event"/>
  <div class="event-notes">
    <xsl:value-of select="key('label','more_info')/@tr"/>:
    <xsl:choose>
      <xsl:when test="$e/@facebook_id!=''">
        <a href="{$e/@link}" target="_blank">evento su Facebook</a>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$e/@link!=''">
          <div><a href="{$e/@link}" target="_blank"><xsl:value-of select="substring($e/@link,0,80)"/></a></div>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="$e/@contact_name!=''">
      <div><xsl:value-of select="$e/@contact_name"/></div>
    </xsl:if>
    <xsl:if test="$e/@email!=''">
      <div><a href="mailto:{$e/@email}"><xsl:value-of select="$e/@email"/></a></div>
    </xsl:if>
  </div>
</xsl:template>


<!-- ###############################
     EVENTS HISTORY
     ############################### -->
<xsl:template name="eventsHistory">
  <xsl:if test="$ei/history">
    <div class="pckbox">
      <h3><xsl:value-of select="key('label','anniversaries')/@tr"/></h3>
      <ul class="items">
        <xsl:for-each select="$ei/history/r_event">
          <li>
            <xsl:attribute name="class"><xsl:value-of select="@type"/>-item<xsl:if test="position()=last()"><xsl:text> last</xsl:text></xsl:if></xsl:attribute>
            <xsl:value-of select="@year"/>: <xsl:value-of select="@description"/>
          </li>
        </xsl:for-each>
      </ul>
    </div>
  </xsl:if>
</xsl:template>


<!-- ###############################
     EVENT INSERT
     ############################### -->
<xsl:template name="eventInsert">
  <xsl:choose>
    <xsl:when test="/root/publish/@id &gt; 0">
      <xsl:value-of select="key('label','insert_thanks')/@tr"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="javascriptForms"/>
      <script type="text/javascript">
      $().ready(function() {
          $("#event-insert").validate({
              rules: {
                  start_date: "required",
                  id_event_type: { required: true, min: 1 },
                  title: "required",
                  contact_name: "required",
                  privacy: "required",
                  name_p: "required",
                  email: { required: true, email: true },
                  email_p: { required: true, email: true }
              },
              messages: {
                  id_event_type: { min: "" },
                  privacy: "E' necessario dare il consenso per poter pubblicare l'evento online"
              }
          });
          $("input#start_date").datepicker({ dateFormat: "dd-mm-yy" });
      });
      </script>
      <p>Gli eventi del calendario vengono approvati a discrezione della redazione del sito, e solo se attinenti ai temi e alle finalità dello <a href="https://www.peacelink.it/peacelink/associazione-peacelink-statuto-2022" target="_blank">statuto associativo di PeaceLink</a>: la promozione della cultura della solidarietà in tutte le sue forme, la difesa dei diritti umani, l'educazione alla pace, la nonviolenza, il disarmo, il coordinamento informativo delle attività di volontariato, la cooperazione internazionale, il supporto ad azioni umanitarie, la sensibilità alle questioni del disagio e della sofferenza, il ripudio del razzismo e della mafia, la difesa dell'ambiente, la cultura della legalità e dei diritti civili, in particolare i diritti telematici, i diritti all'espressione multimediale del pensiero e i diritti al pluralismo informativo.</p>
      <form action="{/root/site/@base}/{/root/site/events/@path}/actions.php" method="post" id="event-insert" accept-charset="{/root/site/@encoding}" enctype="multipart/form-data">
        <input type="hidden" name="from" value="event"/>
        <xsl:if test="/root/topic">
          <input type="hidden" name="id_topic" value="{/root/topic/@id}"/>
        </xsl:if>
        <fieldset>
          <legend><xsl:value-of select="key('label','when')/@tr"/></legend>
          <ul class="form-inputs">
            <li class="form-notes"><xsl:value-of select="key('label','when_desc')/@tr"/></li>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">start_date</xsl:with-param>
              <xsl:with-param name="tr_label" select="key('label','date')/@tr"/>
              <xsl:with-param name="size">small</xsl:with-param>
              <xsl:with-param name="required" select="true()"/>
            </xsl:call-template>
            <li>
              <label for="start_date_h" class="required"><xsl:value-of select="key('label','time')/@tr"/></label>
              <input name="start_date_h" size="2" value="{/root/events/info/time/@h}"/> : <input id="start_date_i" name="start_date_i" size="2" value="{/root/events/info/time/@m}"/>
              <xsl:text> </xsl:text><xsl:value-of select="key('label','time_format')/@tr"/>
            </li>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">length</xsl:with-param>
              <xsl:with-param name="size">small</xsl:with-param>
              <xsl:with-param name="value">1</xsl:with-param>
              <xsl:with-param name="tr_label">Durata (in ore)</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">allday</xsl:with-param>
              <xsl:with-param name="type">checkbox</xsl:with-param>
              <xsl:with-param name="label">all_day_long</xsl:with-param>
            </xsl:call-template>
          </ul>
        </fieldset>
        <fieldset>
          <legend><xsl:value-of select="key('label','what')/@tr"/></legend>
          <ul class="form-inputs">
            <li>
              <label class="required"><xsl:value-of select="key('label','event_type')/@tr"/></label>
              <select name="id_event_type">
                <option value="0"><xsl:value-of select="key('label','choose_option')/@tr"/></option>
                <xsl:for-each select="/root/events/event_types/type">
                  <option value="{@id}"><xsl:value-of select="@type"/></option>
                </xsl:for-each>
              </select>
            </li>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">title</xsl:with-param>
              <xsl:with-param name="required" select="true()"/>
            </xsl:call-template>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">description</xsl:with-param>
              <xsl:with-param name="type">textarea</xsl:with-param>
            </xsl:call-template>
          </ul>
        </fieldset>
        <fieldset>
          <legend><xsl:value-of select="key('label','where')/@tr"/></legend>
          <ul class="form-inputs">
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">address</xsl:with-param>
              <xsl:with-param name="label">address</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">place</xsl:with-param>
              <xsl:with-param name="label">town</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="formInputGeo"/>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">place_details</xsl:with-param>
              <xsl:with-param name="label">address_notes</xsl:with-param>
              <xsl:with-param name="type">textarea</xsl:with-param>
            </xsl:call-template>
          </ul>
        </fieldset>
        <fieldset>
          <legend>Organizzazione</legend>
          <ul class="form-inputs">
            <li class="form-notes">Nome, email e link dell'organizzazione vengono pubblicati sul calendario. Non inserire la propria email personale se non la si vuole rendere pubblica. Il telefono è solo ad uso interno per eventuali verifiche da parte di PeaceLink.</li>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">contact_name</xsl:with-param>
              <xsl:with-param name="label">contact_main</xsl:with-param>
              <xsl:with-param name="required" select="true()"/>
            </xsl:call-template>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">email</xsl:with-param>
              <xsl:with-param name="tr_label">Email dell'organizzatore</xsl:with-param>
              <xsl:with-param name="required" select="true()"/>
            </xsl:call-template>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">link</xsl:with-param>
              <xsl:with-param name="label">link_one</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="formInput">
              <xsl:with-param name="varname">phone</xsl:with-param>
            </xsl:call-template>
          </ul>
        </fieldset>
        <fieldset>
          <legend>Immagine associata</legend>
          <input type="hidden" name="MAX_FILE_SIZE" value="{/root/site/@max_upload_size_pub}"/>
          <ul class="form-inputs">
            <li class="form-notes">Non sono ammessi file di dimensioni superiori a <xsl:value-of select="/root/site/@max_upload_size_pub div 1024 div 1024"/> MB. Si accettano immagini solo in formato JPG</li>
            <li><input type="file" name="img[]" size="50" class="upload"/></li>
          </ul>
        </fieldset>
        <fieldset>
          <legend>Segnalato da</legend>
            <ul class="form-inputs">
              <li class="form-notes">Nome e email di chi segnala l'evento non vengono mostrate pubblicamente, sono solo ad uso interno per eventuali verifiche da parte di PeaceLink.</li>
              <xsl:call-template name="formInput">
                <xsl:with-param name="label">name</xsl:with-param>
                <xsl:with-param name="varname">name_p</xsl:with-param>
                <xsl:with-param name="value" select="/root/user/@name"/>
                <xsl:with-param name="tr_label">Nome e cognome</xsl:with-param>
                <xsl:with-param name="required" select="true()"/>
                <xsl:with-param name="disabled" select="/root/user/@name!='' "/>
              </xsl:call-template>
              <xsl:call-template name="formInput">
                <xsl:with-param name="label">email</xsl:with-param>
                <xsl:with-param name="varname">email_p</xsl:with-param>
                <xsl:with-param name="value" select="/root/user/@email"/>
                <xsl:with-param name="required" select="true()"/>
                <xsl:with-param name="disabled" select="/root/user/@email!='' "/>
              </xsl:call-template>
              <xsl:call-template name="formInput">
                <xsl:with-param name="varname">contact</xsl:with-param>
                <xsl:with-param name="type">checkbox</xsl:with-param>
                <xsl:with-param name="label">contact_warning</xsl:with-param>
                <xsl:with-param name="value" select="true()"/>
              </xsl:call-template>
            </ul>
        </fieldset>
        <ul class="form-inputs">
          <xsl:if test="/root/site/@captcha">
            <li class="clearfix">
              <xsl:call-template name="captchaWrapper"/>
            </li>
          </xsl:if>
          <xsl:call-template name="privacyWarning">
            <xsl:with-param name="node" select="/root/privacy_warning"/>
          </xsl:call-template>
          <li class="buttons"><input type="submit" value="{key('label','submit')/@tr}"/></li>
        </ul>
      </form>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- ###############################
     EVENT SEARCH
     ############################### -->
<xsl:template name="eventSearch">
  <xsl:if test="/root/events/future/@q='' and /root/events/future/@id_geo &gt; 0">
    <xsl:variable name="province" select="/root/geo/provinces/province[@id=/root/events/future/@id_geo]"/>
    <xsl:variable name="region" select="/root/geo/regions/region[@id=$province/@id_reg]"/>
    <p>Puoi iscriverti al calendario degli eventi della provincia di <xsl:value-of select="$province/@name"/> cliccando su questo link: <a href="webcal://cal.peacelink.it/ical/prov/{$province/@short}">webcal://cal.peacelink.it/ical/prov/<xsl:value-of select="$province/@short"/></a><br/>
    oppure qui per la regione <xsl:value-of select="$region/@name"/>: <a href="webcal://cal.peacelink.it/ical/reg/{$region/@name}">webcal://cal.peacelink.it/ical/reg/<xsl:value-of select="$region/@name"/></a>
    </p>
  </xsl:if>
  <ul class="tabs">
    <li><a data-count="{/root/events/future/items/@tot_items}" href="#future"><xsl:value-of select="/root/events/future/items/@tot_items"/> eventi futuri</a></li>
    <li><a data-count="{/root/events/past/items/@tot_items}" href="#past"><xsl:value-of select="/root/events/past/items/@tot_items"/> eventi passati</a></li>
  </ul>
  <div id="future" class="event-search">
    <xsl:choose>
      <xsl:when test="/root/events/future/items/@tot_items &gt; 0">
        <xsl:call-template name="eventsSearchResults">
          <xsl:with-param name="root" select="/root/events/future/items"/>
          <xsl:with-param name="node" select="/root/events/future"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="/root/events/future/@id_geo &gt; 0">
          <p><h2><xsl:value-of select="/root/geo/provinces/province[@id=/root/events/future/@id_geo]/@name"/></h2></p>
        </xsl:if>
        <p>Non ci sono eventi programmati nei prossimi giorni che corrispondono alla tua ricerca.</p>
        <p>Puoi consultare il <a href="/calendario/events.php">calendario generale</a> e la <a href="/calendario/map.php">mappa di tutte le iniziative in Italia</a></p>
      </xsl:otherwise>
    </xsl:choose>
  </div>
  <div id="past" class="event-search">
    <xsl:call-template name="eventsSearchResults">
      <xsl:with-param name="root" select="/root/events/past/items"/>
      <xsl:with-param name="node" select="/root/events/past"/>
    </xsl:call-template>
  </div>
</xsl:template>


<!-- ###############################
     EVENTS SEARCH RESULTS
     ############################### -->
<xsl:template name="eventsSearchResults">
  <xsl:param name="root"/>
  <xsl:param name="node"/>
  <xsl:param name="class2"/>
  <xsl:if test="$root/@tot_items">
    <xsl:call-template name="paging">
      <xsl:with-param name="currentPage" select="$root/@page"/>
      <xsl:with-param name="totalPages" select="$root/@tot_pages"/>
      <xsl:with-param name="totalItems" select="$root/@tot_items"/>
      <xsl:with-param name="label" select="$root/@label"/>
      <xsl:with-param name="type" select="'header'"/>
      <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>
    <ul class="items {$class2}">
      <xsl:apply-templates mode="searchresults" select="$root/item"/>
    </ul>
    <xsl:call-template name="paging">
      <xsl:with-param name="currentPage" select="$root/@page"/>
      <xsl:with-param name="totalPages" select="$root/@tot_pages"/>
      <xsl:with-param name="totalItems" select="$root/@tot_items"/>
      <xsl:with-param name="label" select="$root/@label"/>
      <xsl:with-param name="type" select="'footer'"/>
      <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>


<!-- ###############################
     EVENT SEARCH ITEM
     ############################### -->
<xsl:template mode="searchresults" match="item">
  <xsl:variable name="e" select="."/>
  <li>
    <xsl:attribute name="class"><xsl:value-of select="@type"/>-item<xsl:if test="position()=last()"><xsl:text> last</xsl:text></xsl:if></xsl:attribute>
    <xsl:if test="@id &gt; 0"><xsl:attribute name="id"><xsl:value-of select="@type"/>-<xsl:value-of select="@id"/></xsl:attribute></xsl:if>
    <xsl:if test="@hdate"><div class="hdate">[<xsl:value-of select="@hdate"/>]</div></xsl:if>
    <div class="event-wrap">
      <div class="date">
        <div class="date-month"><xsl:value-of select="$e/@month_short"/></div>
        <div class="date-day"><xsl:value-of select="$e/@day"/></div>
        <div class="date-wday">
          <xsl:choose>
            <xsl:when test="$pagetype='events' and $subtype='search'"><xsl:value-of select="$e/@year"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="$e/@day_short"/></xsl:otherwise>
          </xsl:choose>
        </div>
      </div>
      <xsl:if test="$e/image">
        <div class="image-wrap">
          <a title="{$e/@title}">
            <xsl:attribute name="href">
              <xsl:call-template name="createLinkUrl">
                <xsl:with-param name="node" select="$e"/>
              </xsl:call-template>
            </xsl:attribute>
            <xsl:call-template name="imageItem">
              <xsl:with-param name="id" select="$e/image/@id"/>
              <xsl:with-param name="width" select="'100'"/>
              <xsl:with-param name="format" select="$e/image/@format"/>
              <xsl:with-param name="ratio" select="$e/image/@image_ratio"/>
              <xsl:with-param name="caption" select="$e/@title"/>
              <xsl:with-param name="path" select="'events'"/>
            </xsl:call-template>
          </a>
        </div>
      </xsl:if>

      <div class="event-type"><xsl:value-of select="$e/@event_type"/></div>
      <h3>
        <xsl:call-template name="createLink">
          <xsl:with-param name="name"><xsl:value-of select="$e/@title" disable-output-escaping="yes"/></xsl:with-param>
          <xsl:with-param name="node" select="$e"/>
        </xsl:call-template>
      </h3>
      <xsl:if test="$e/@place!=''">
        <div class="event-place">
          <xsl:value-of select="$e/@place"/>
          <xsl:if test="$e/@geo_name!=''"><xsl:value-of select="concat(' (',$e/@geo_name,')')"/></xsl:if>
        </div>
      </xsl:if>
    </div>
  </li>
</xsl:template>


<!-- ###############################
     EVENTS INFO
     ############################### -->
<xsl:template name="eventsInfo">
  <div id="events-info">

    <xsl:if test="$ei/insert">
      <div id="submit-event" class="pckbox">
        <h3 class="icon"><xsl:value-of select="key('label','event_submit')/@tr"/></h3>
        <div>
          <xsl:call-template name="createLink">
            <xsl:with-param name="node" select="$ei/insert"/>
            <xsl:with-param name="name" select="key('label','event_submit_click')/@tr"/>
          </xsl:call-template>
        </div>
      </div>
    </xsl:if>

    <div id="events-map" class="pckbox">
      <h3 class="icon">Mappa</h3>
      <a href="/calendario/map.php" title="Mappa degli eventi dei prossimi giorni">
        <img src="https://cdn.peacelink.it/images/26219.jpg?format=jpg&amp;w=400" alt="Eventi dei prossimi giorni"/>
      </a>
    </div>

    <div id="search-event" class="pckbox">
      <h3 class="icon">
        <xsl:call-template name="createLink">
          <xsl:with-param name="node" select="$ei/search"/>
          <xsl:with-param name="name" select="'Ricerca eventi'"/>
        </xsl:call-template>
      </h3>
      <form method="get" id="event-search-form" accept-charset="{/root/site/@encoding}">
        <xsl:attribute name="action">
          <xsl:call-template name="createLinkUrl">
            <xsl:with-param name="node" select="$ei/search"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:if test="$preview='1'">
          <input type="hidden" name="id_type" value="5"/>
          <input type="hidden" name="subtype" value="search"/>
        </xsl:if>
        <ul class="form-inputs">
          <xsl:call-template name="formInput">
            <xsl:with-param name="varname">q</xsl:with-param>
            <xsl:with-param name="tr_label">Testo</xsl:with-param>
            <xsl:with-param name="value" select="/root/events/future/@q"/>
          </xsl:call-template>
          <li>
            <label for="id_geo">Provincia</label>
            <xsl:call-template name="geoSelect">
              <xsl:with-param name="currentGeo" select="/root/events/future/@id_geo" />
              <xsl:with-param name="chooseOption" select="'-- tutte --'" />
            </xsl:call-template>
          </li>          
          <li class="buttons"><input type="submit" value="{key('label','search')/@tr}"/></li>
        </ul>
      </form>
    </div>

    <xsl:call-template name="ricorrenze"/>
    
    <!--
    <div class="pckbox">
      <h3>Export</h3>
    </div>
    -->

  </div>

</xsl:template>


<!-- ###############################
     NEXT EVENTS
     ############################### -->
<xsl:template name="nextEvents">
<xsl:apply-templates select="/root/c_features/feature[@id_user='0' and @id_function='4']"/>
</xsl:template>


<!-- ###############################
     RICORRENZE
     ############################### -->
<xsl:template name="ricorrenze">
  <xsl:if test="not(/root/topic) and /root/c_features/feature[@id=125]/items/item">
    <div id="ricorrenze" class="pckbox">
      <xsl:apply-templates select="/root/c_features/feature[@id=125]">
        <xsl:with-param name="title_class" select="'icon'"/>
      </xsl:apply-templates>
    </div>
  </xsl:if>
</xsl:template>


</xsl:stylesheet>
