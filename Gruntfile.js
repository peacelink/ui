'use strict';

module.exports = function(grunt) {

  var cssFiles = [
    'css/pck.css'
  ];

  var jsFiles = [
    'js/main.js'
  ];

  grunt.initConfig({

    cssmin: {
      target: {
        files: [{
          'cdn/dist/pck.min.css': cssFiles
        }]
      }
    },
    
    // Minify the JS
    uglify: {
      dist: {
        files: {
          'cdn/dist/main.min.js': jsFiles
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify-es');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  
  grunt.registerTask('default', [ 'cssmin', 'uglify' ]);
};
