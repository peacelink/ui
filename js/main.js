var script = document.getElementById('pckjs');
var scriptSrc = script.src;
var scriptParser = document.createElement('a');
scriptParser.href = script.src;
var scriptOrigin = scriptParser.origin;
var eventsList = 'https://www.peacelink.it/js/events.php';

$(function() {

  var isDesktop = $("#fotonotizia").css("float") == "right";
  
  // shorten links
  /*
  var maxLinkText = isDesktop? 80 : 35;
  $('#article-content a').html(function(_,txt) {
    return txt.length>maxLinkText? txt.substr(0,maxLinkText-3)+'...':txt;
  });
  */
  
  // map expand
  $(".subtopics-header").click(function () {
    $link = $(this);
    $content = $link.parent().next();
    if($content.css('display')=='none') {
      $(".subtopics").slideUp("slow");
      $(".items").slideUp("slow");
      $(".subtopics-header").removeClass('fa-minus-square').addClass('fa-plus-square');
    }
    $content.slideToggle("slow");
    $link.toggleClass('fa-plus-square fa-minus-square');
  });
  $(".latest-header").click(function () {
    $link = $(this);
    $container = $link.parent();
    $content = $container.next();
    if($content.css('display')=='none') {
      $(".subtopics").slideUp("slow");
      $(".items").slideUp("slow");
      $(".latest-header").removeClass('fa-minus-square').addClass('fa-plus-square');
    }
    $content.slideToggle("slow");
    $link.toggleClass('fa-plus-square fa-minus-square');
  });

  // validation
  $.validator.setDefaults({
    errorClass: "invalid",
    errorPlacement: function(error, element) {
      error.insertAfter(element);
      element.parent("li").addClass("invalid");
    }
  })

  // org keywords
  $("#orgs-insert ul#keywords-tree li ul li .k-checkbox").change(function() {
    if($(this).is(":checked")) {
      $(this).parents("li:eq(1)").children(".k-checkbox").prop("checked", true);
    } else {
      var count_siblings = $(this).parents("li:eq(1)").find("ul li .k-checkbox:checked").length;
      if (count_siblings==0) {
        $(this).parents("li:eq(1)").children(".k-checkbox").prop("checked", false);
      }
    }
  });
  
  $("#ucontact").validate({
    rules: {
      name: "required",
      email: { required: true, email: true },
      comments: "required",
    }
  });

  // Close Highslide on external click
  $(document).mouseup(function (e) {
    if($(".highslide-wrapper").length==0)
      return;

    var container = $(".highslide-wrapper");

    if (!container.is(e.target) && container.has(e.target).length === 0) {
      for (var i = 0; i < hs.expanders.length; i++) {
        var exp = hs.expanders[i];
        if (exp) exp.close();
      }
    }
  });

  // Tabs
  $('ul.tabs').each(function(){
    var active, content, links = $(this).find('a');
    var urlParams = getUrlParams(location.search);
    active = $(links.filter('[href="'+location.hash+'"]')[0] || links.filter('[href="#'+urlParams.tab+'"]')[0] || links[0]);
    active.addClass('active');

    content = $(active[0].hash);

    links.not(active).each(function () {
      $(this.hash).hide();
    });

    $(this).on('click', 'a', function(e){
      active.removeClass('active');
      content.hide();
      active = $(this);
      content = $(this.hash);
      active.addClass('active');
      content.show();
      e.preventDefault();
    });
  });

  // Slideshow
  $('.slideshow').bbslider({
    auto: true,
    controls: true,
    timer: 3000,
    autoHeight: false,
    loop: true,
    pauseOnHit: false,
    controlsText:[
      '<a class="prev control fa fa-angle-left" href="#"></a>',
      '<a class="next control fa fa-angle-right" href="#"></a>'],
    touch: true
  });
  $( ".bbslider-wrapper" ).hover(function() {
    $('.prev-control-wrapper').css({ opacity: 1});
    $('.next-control-wrapper').css({ opacity: 1});
  });
  
  // Counter trigger
  if($('#pck-counter').length && $('#pck-counter').is(":visible")) {
    var totCounters = 4;
    var counterRandomInt = Math.floor(Math.random() * Math.floor(totCounters)) + 1;
    pckCounter(counterRandomInt);
  }

  // sociale.network tag for landing pages
  if($("body.subtype-keyword")[0]) {
    keywordClass = $('#main-wrap').attr('class');
    if(keywordClass!='' && keywordClass.startsWith('k-')) {
      keyword = keywordClass.substr(2)
      if (keyword=='albert') {
        keyword = 'pcknews';
      }
      var mapi = new MastodonApi({
        target_selector: '#socialenetwork'
      },'https://labs.peacelink.it/sociale/tag.php?tag='+keyword);
    }  
  } else {
    var mapi = new MastodonApi({
      target_selector: '#socialenetwork'
    },'https://labs.peacelink.it/sociale/api.php');
  }

  // Map26Feb
  if ($("body.article#id49004")[0] ) {
    eventsList = 'https://www.peacelink.it/js/events.php?start=2022-02-26&end=2022-02-27';
    loadGoogleMap('events');
  }

  // Calendar
  var calendarEl = document.getElementById('calendar');
  var smallScreen = window.innerWidth <= 800;

  var calendar = new FullCalendar.Calendar(calendarEl, {
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listMonth'
    },
    defaultView: 'listMonth',
    viewSkeletonRender: function(info) {
      if(info.view.type=='listMonth') {
        $("#next-events").removeClass('hidden');
        calendarMasonry();
      } else {
        $("#next-events").addClass('hidden');
      }
    },
    minTime: '08:00:00',
    editable: false,
    navLinks: true,
    eventLimit: true,
    eventRender: function(info) {
      switch(info.view.type) {
        case 'listMonth':
          var element = info.el.querySelector('.fc-list-item-title');
          var event = document.createElement('td');
          event.className = 'fc-list-item-title fc-widget-content';
          if(info.event.extendedProps.image) {
            var eventImage = document.createElement('img');
            eventImage.setAttribute('src',info.event.extendedProps.image);
            eventImage.setAttribute('width','200');
            eventImage.className = 'left';
            event.appendChild(eventImage);
          }
          var eventType = document.createElement('div');
          eventType.className = 'event-type';
          eventType.innerText = info.event.extendedProps.type;
          event.appendChild(eventType);
          var eventTitle = document.createElement('h3');
          if(info.event.url) {
            var eventLink = document.createElement('a');
            eventLink.setAttribute('href',info.event.url);
            eventLink.innerText = info.event.title;
            eventTitle.appendChild(eventLink);
          } else {
            eventTitle.innerText = info.event.title;
          }
          event.appendChild(eventTitle);
          if(info.event.extendedProps.place!='') {
            var place = document.createElement('div');
            place.innerText = info.event.extendedProps.place + ' (' + info.event.extendedProps.prov + ')';
            event.appendChild(place);
          }
          element.replaceWith(event);
          break;
        case 'agendaWeek':
        case 'agendaDay':
          var element = info.el.querySelector('.fc-content');
          var event = document.createElement('div');
          event.className = 'fc-content';
          if(info.view.type == 'agendaDay') {
            if(info.event.extendedProps.thumb) {
              var eventImage = document.createElement('img');
              eventImage.setAttribute('src',info.event.extendedProps.thumb);
              eventImage.setAttribute('width','120');
              event.appendChild(eventImage);
            }
          }
          var eventTitle = document.createElement('h4');
          eventTitle.className = 'fc-title';
          if(info.event.url) {
            var eventLink = document.createElement('a');
            eventLink.setAttribute('href',info.event.url);
            eventLink.innerText = info.event.title;
            eventTitle.appendChild(eventLink);
          } else {
            eventTitle.innerText = info.event.title;
          }
          event.appendChild(eventTitle);
          if(info.event.extendedProps.place!='') {
            var place = document.createElement('div');
            place.innerText = '';
            if(info.view.type == 'agendaDay') {
              place.innerText = info.event.extendedProps.address + ' - ';
            }
            place.innerText += info.event.extendedProps.place + (info.event.extendedProps.prov!=null? ' (' + info.event.extendedProps.prov + ')':'');
            event.appendChild(place);
          }
          element.replaceWith(event);
          break;
        case 'month':
          var element = info.el.querySelector('.fc-content');
          break;
      }
    },
    eventClick: function(info) {
      info.jsEvent.preventDefault();
      eventDialog(info.event);
      history.pushState({}, '', info.event.url);
    },
    events: {
      url: '/js/events.php'
    }
  });

  if(!smallScreen && calendarEl) {

    calendar.render();
    calendar.setOption('locale', 'it');

    // check if event data is present
    var eventEl = document.getElementById('event-content');
    var h1 = document.getElementsByTagName('h1');
    if(h1[0] && eventEl.contains(h1[0]) && h1[0].innerText.trim()!='') {
      calendar.changeView('agendaDay',h1[0].getAttribute('data-start'));
      $('#event-content').modal();
      $('#event-content').on($.modal.CLOSE, function(event, modal) {
        document.title = 'Calendario di PeaceLink';
      });
    }
    
    window.onpopstate = function(event) {
      window.location.reload();
    };
  }

});


function loadGoogleMap(map_type) {
  const gkey = 'AIzaSyB_wFQv8dbfu-9yGE6fqPqxxfa0wWAOng8';
  var s = document.createElement("script");
  s.type = "text/javascript";
  switch(map_type) {
    case 'events':
      s.src  = 'https://maps.googleapis.com/maps/api/js?key=' + gkey + '&callback=initGoogleMapEvents&libraries=&v=weekly&libraries=marker&region=IT&language=it';
    break;
    case 'orgs':
      s.src  = 'https://maps.googleapis.com/maps/api/js?key=' + gkey + '&callback=initGoogleMapOrgs&libraries=&v=weekly&libraries=marker&region=IT&language=it';
    break;
  }
  $("head").append(s);
}

// Google Map
function initGoogleMapEvents() {
    const rome = { lat: 41.902782, lng: 12.496366 };
    var map = new google.maps.Map(document.getElementById("google-map"), {
        mapId: "7e9286c56a397",
        mapTypeControl: false,
        zoom: 6,
        center: rome,
    });
    $.ajax({
        type: "GET",
        dataType: "json",
        url: eventsList,
        success: function(data) {
            var infowindow = new google.maps.InfoWindow();
            for(var i = 0; i < data.length; i++) {
                var event = data[i];
                var latitude = Number(event.latitude);
                var longitude = Number(event.longitude);
                if(latitude != 0 && longitude != 0) {
                    var marker = new google.maps.marker.AdvancedMarkerElement({
                        map: map,
                        position: { lat: latitude, lng: longitude },
                        title: event.title
                    });
                    var event_location = event.address + ', ' + event.place
                    var event_content = '<b><a href="' + event.url + '" target="_blank">' + event.title + '</a></b>';
                    event_content += '<br/>' + event.start_date + ' ' + event.start_time;
                    event_content += '<br/>' + event_location;
                    if(event.image) {
                      event_content += '<br/><a href="' + event.url + '" target="_blank"><img src="' + event.image + '" width="100"></a>';
                    }
                    makeInfoWindowEvent(map, infowindow, event_content, marker);
                }
            }
        }
    });
}

// Map markers content
function makeInfoWindowEvent(map, infowindow, contentString, marker) {
    google.maps.event.addListener(marker, 'click', function() {
    	infowindow.setContent(contentString);
    	infowindow.open(map, marker);
    });
}

function initGoogleMapOrgs() {
  const rome = { lat: 41.902782, lng: 12.496366 };
  var map = new google.maps.Map(document.getElementById("google-map"), {
      mapId: "7e9286c56a397",
      mapTypeControl: false,
      zoom: 6,
      center: rome,
  });
  $.ajax({
      type: "GET",
      dataType: "json",
      url: 'https://www.peacelink.it/js/orgs.php',
      success: function(data) {
          var infowindow = new google.maps.InfoWindow();
          for(var i = 0; i < data.length; i++) {
              var org = data[i];
              var latitude = Number(org.latitude);
              var longitude = Number(org.longitude);
              if(latitude != 0 && longitude != 0) {
                  var marker = new google.maps.marker.AdvancedMarkerElement({
                      map: map,
                      position: { lat: latitude, lng: longitude },
                      title: org.name
                  });
                  var org_location = org.address + ', ' + org.town
                  var org_content = '<b><a href="' + org.url + '" target="_blank">' + org.name + '</a></b>';
                  org_content += '<br/>' + org_location;
                  if(org.thumb) {
                    org_content += '<br/><a href="' + org.url + '" target="_blank"><img src="' + org.thumb + '" width="100"></a>';
                  }
                  makeInfoWindowEvent(map, infowindow, org_content, marker);
              }
          }
      }
  });
}

// MASONRY
function calendarMasonry() {
  $('.events-grid').masonry({
    itemSelector: '.event-item',
    percentPosition: true,
    gutter: 20
  });
}

// HELPERS
/**
 * getUrlParams
 * Accepts either a URL or querystring and returns an object associating 
 * each querystring parameter to its value. 
 *
 * Returns an empty object if no querystring parameters found.
 */
function getUrlParams(urlOrQueryString) {
  if ((i = urlOrQueryString.indexOf('?')) >= 0) {
    var queryString = urlOrQueryString.substring(i+1);
    if (queryString) {
      return mapUrlParams(queryString);
    } 
  }

  return {};
}

/**
 * mapUrlParams
 * Helper function for `getUrlParams()`
 * Builds the querystring parameter to value object map.
 *
 * @param queryString {string} - The full querystring, without the leading '?'.
 */
function mapUrlParams(queryString) {
  return queryString
    .split('&')
    .map(function(keyValueString) { return keyValueString.split('=') })
    .reduce(function(urlParams, [key, value]) {
      if (Number.isInteger(parseInt(value)) && parseInt(value) == value) {
        urlParams[key] = parseInt(value);
      } else {
        urlParams[key] = decodeURI(value);
      }
      return urlParams;
    }, {});
}

// RSS
function rssListLoad(divId,rssUrl,host) {
  var parameters="url="+encodeURIComponent(rssUrl)+"&ttl=30"
  var url='/js/rss_fetch.php?'+parameters
  $.get(url, function(data) {
    var $XML = $(data);
    var mList = $('<ul/>')
    $XML.find("item").each(function() {
        var $this = $(this),
            item = {
                title:       $this.find("title").text(),
                link:        $this.find("link").text(),
                description: $this.find("description").text()
            };
        if(item.title && item.link) {
          mList.append($('<li/>').html('<a href="'+item.link+'">'+item.title+'</a><div>'+item.description+'</div>'));
        }
    });
    $('#'+divId).append(mList)
    if($('#'+divId+' li').length) {
      $('#'+divId).easyTicker({
        direction: 'up',
        speed: 'slow',
        interval: 4000,
        height: 200,
        visible: 1,
        mousePause: 1
      })  
    } else {
      $('#'+divId).html('Non ci sono messaggi')
    }
  });
}

// ASYNC HTML LOAD
function htmlLoad(divId,htmlUrl,cache,append,isClass) {
  append = append || false
  isClass = isClass || false
  $.ajax({
    url : htmlUrl,
    type : "GET",
    cache : cache,
    dataType: "html",
    success : function(data) {
      if(data) {
        if(append) {
          $((isClass?'.':'#')+divId).append(data);
        } else {
          $((isClass?'.':'#')+divId).html(data);
        }
      } else {
        console.log('No data from ' + htmlUrl)
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) { 
      console.log("Response: " + XMLHttpRequest.responseText + ' - ' + errorThrown);
    }
  });
}

// HIGHSLIDE
hs.graphicsDir = 'https://www.peacelink.it/js/highslide/graphics/';
hs.showCredits = false;
hs.outlineType = 'rounded-white';
hs.wrapperClassName = 'highslide-white';
hs.fullExpandTitle = 'Espandi alle dimensioni reali';
hs.loadingText = 'Caricamento...';
hs.loadingTitle = 'Clicca per interrompere';
hs.restoreTitle = 'Clicca per chiudere l\'immagine o per trascinarla';


// OLD STUFF

var phpeace = {
  pub_web : 'www.peacelink.it'
};


// ARTICLES BOXES POPUP 
function open_popup(src,width,height) {
  if ( document.all || document.getElementById || document.layers ) { if ( width>screen.width ) { width = screen.width - 200; } }
  features = 'location=0,statusbar=0,menubar=0,scrollbars=1,width=' + width +',height=' + height + ',screenX=100,screenY=100';
  var theWindow = window.open(src.getAttribute('href'), src.getAttribute('target') || '_blank', features);
  theWindow.focus();
  return theWindow;
}


// POLLS
function styleToggle(b) {
  for (var i = 0; i< b.form.length; i++) {
  if (b.form[i].name == b.name) {
      b.form[i].parentNode.style.fontWeight = b.form[i].checked? 'bold' : '';
    }
  }
}


String.prototype.htmlEntities = function () {
   return this.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
};

function youtubeVideo(i, videoItem) {
  var myDate = new Date(videoItem.snippet.publishedAt);
  var displayDate = myDate.toLocaleDateString('it-it', {day: '2-digit', month: 'long', year: 'numeric'});
  video = '<li>';
  video += '<div id="youtube-video'+i+'" class="youtube-video"></div>';
  video += '<div>' + displayDate + '</div>';
  video += '<h3>' + videoItem.snippet.title + '</h3>';
  video += '<div class="description">' + videoItem.snippet.description + '</div>';
  video += '</li>';
  return video;
};

// COUNTERS
var dateObj = new Date();
var firstJan = new Date(dateObj.getFullYear(), "0", "1");
var secondsUpToNow = dateObj.getTime() / 1000 - firstJan.getTime() / 1000;
var secondsYear = 365 * 24 * 3600;
var counter = null;
var seconds = 0;

function pckCounter(counterId) {
  pckCounterReset();
  switch(counterId) {
    case 1:
      pckCounter1(747000000,0,9,'euro spesi dall\'inizio di quest\'anno per i cacciabombardieri F35','https://www.peacelink.it/disarmo/a/46129.html');
    break;
    case 2:
      pckCounter2(2500000,'bambini sotto i 5 anni morti per malnutrizione dall\'inizio di quest\'anno','https://www.peacelink.it/pace/a/46664.html');
    break;
    case 3:
      pckCounter3(85000/3,'bambini sotto i 5 anni morti di fame in Yemen dall\'inizio dell\'anno per colpa della guerra. ','https://www.theguardian.com/world/2018/nov/21/yemen-young-children-dead-starvation-disease-save-the-children');
    break;
    case 4:
      pckCounter4(275000000,'armi italiane all\'Arabia Saudita dall\'inizio di quest\'anno','https://www.peacelink.it/disarmo/a/46625.html');
    break;
  }
}

function pckCounterReset() {
  $('#pck-counter').html('');
}

function pckCounter1(numberPerYear,decimals,maxDigits,text,link) {
  var odometer = $('<div/>');
  odometer.addClass('odometer-container');
  odometer.append($('<div class="odometer"/>'));
  $('#pck-counter').append(odometer)
  if($('#pck-counter .odometer').length) {
    var numberNow = (numberPerYear * secondsUpToNow / secondsYear).toFixed(decimals);
    var increment = numberPerYear / secondsUpToNow;
    $('.odometer').jOdometer({
      increment: increment, 
      counterStart:numberNow, 
      delayTime: 1000,
      speed: 1000,
      counterEnd:false, 
      formatNumber:true, 
      numbersImage: scriptOrigin + '/js/images/jodometer-numbers-color.png',
      maxDigits: maxDigits,
      spaceNumbers: 0
    });
  $('#pck-counter').append($('<div class="counter-info"/>').html('<a href="'+link+'">'+text+'</a>'))
  }
}

function pckCounter2(numberPerYear,text,link) {
  var numberNow = (numberPerYear * secondsUpToNow / secondsYear);
  var numberNowDisplay = numberNow | 0;
  var numberNowPartial = numberNow % 1;
  var seconds = secondsYear / (numberPerYear);
  $('#pck-counter').addClass('progresscircle');
  pckCounter2Recurse(numberNowDisplay,numberNowPartial,seconds,text,link,50);
}
function pckCounter2Recurse(numberNowDisplay,numberNowPartial,seconds,text,link,strokeWidth) {
  pckCounterReset();
  counter = new ProgressBar.Circle('#pck-counter', {
      color: '#BBBBBB',
      trailColor: '#FFFFFF',
      easing: 'linear',
      strokeWidth: strokeWidth,
      duration: 1000,
      svgStyle: {height: '100%'},
      text: {
        value: '<a href="'+link+'"><div class="value">'+numberNowDisplay+'</div><div class="description"> '+text+'</div></a>',
        className: 'counter-info',
        style: null
      }
  });
  var animationLength = seconds
  var secondsLeft = seconds
  if(numberNowPartial>0) {
    counter.set(numberNowPartial);
    animationLength = seconds * (1-numberNowPartial)
  }
  counter.animate(1, {
    duration: animationLength * 1000 //ms
  }, function() {
    // increment value by 1
    numberNowDisplay += 1;
    pckCounter2Recurse(numberNowDisplay,0,seconds,text,link,strokeWidth);
  });
}

function pckCounter3(number1PerYear,text1,link) {
  var number1Now = (number1PerYear * secondsUpToNow / secondsYear);
  var number1NowDisplay = number1Now | 0;
  var number1NowPartial = number1Now % 1;
  $('#pck-counter').html('<div class="counter-info counter3"><a href="'+link+'"><div class="value" id="value1">'+number1NowDisplay+'</div><div class="description" id="desc1"> '+text1+'<span class="timer2"></span></div></a></div>');
  var cancel = setInterval( function() {
    var seconds = secondsYear / (number1PerYear);
    var date2Obj = new Date();
    secondsUpToNow = date2Obj.getTime() / 1000 - firstJan.getTime() / 1000;
    secondsLeft = Math.floor(seconds - (secondsUpToNow % seconds));
    minutesLeft = Math.floor(secondsLeft/60);
    secondsLeft -= minutesLeft * 60;      
    $('#pck-counter .timer2').html('Il prossimo tra '+ minutesLeft+':'+(secondsLeft < 10 ? '0'+secondsLeft : secondsLeft))
  }, 1000);
}

function pckCounter4(numberPerYear,text,link) {
  var numberNow = (numberPerYear * secondsUpToNow / secondsYear);
  var numberNowDisplay = numberNow | 0;
  var numberNowPartial = numberNow % 1;
  $('#pck-counter').html('<div class="counter-info counter4"><a href="'+link+'"><div class="value" id="value1">'+numberNowDisplay.toLocaleString('it-IT',{ style: 'currency', currency: 'EUR' })+'</div><div class="description" id="desc1"> '+text+'<span class="timer2"></span></div></a></div>');
  var cancel = setInterval( function() {
    var date2Obj = new Date();
    secondsUpToNow = date2Obj.getTime() / 1000 - firstJan.getTime() / 1000;
    numberNow = ((numberPerYear * secondsUpToNow / secondsYear)).toLocaleString('it-IT',{ style: 'currency', currency: 'EUR' });
    $('#pck-counter #value1').html(numberNow)
  }, 100);
}

function eventDialog(event) {
  var prevTitle = $('.fc-center h2').text();
  document.title = event.title + ' - ' + event.extendedProps.start_date;
  $('#event-content').html('');
  $('#event-content').append('<div class="event-type">' + event.extendedProps.type + '</div>');
  if(event.extendedProps.image) {
    $('#event-content').append('<img class="right" src="' + event.extendedProps.image + '"></img>');
  }
  $('#event-content').append('<h1>' + event.title + '</h1>');
  $('#event-content').append('<div class="event-time">' + event.extendedProps.start_date);
  if(!event.allDay) {
    $('#event-content').append(event.extendedProps.start_time);
    if(event.extendedProps.length > 0) {
      $('#event-content').append(' (' + event.extendedProps.length + ' ore)');
    }
  }
  $('#event-content').append('</div>');
  if(event.extendedProps.place && event.extendedProps.place!='') {
    $('#event-content').append('<div class="event-place icon"><a href="' + event.extendedProps.gmaps + '" target="_blank">' + event.extendedProps.address  + ' - ' + event.extendedProps.place + (event.extendedProps.prov!=null? ' (' + event.extendedProps.prov + ')':'') + '<div>' + event.extendedProps.place_details + '</div></a></div>');  
  }
  $('#event-content').append('<div class="event-desc">' + event.extendedProps.description + '</div>');
  $('#event-content').append('<div class="event-notes">Per maggiori informazioni:');
  $('#event-content').append('<div>' + event.extendedProps.contact_name + '</div>');
  if(event.extendedProps.email && event.extendedProps.email!='') {
    $('#event-content').append('<div><a href="mailto:' + event.extendedProps.email + '">'  + event.extendedProps.email + '</a></div>');
  }
  if(event.extendedProps.link && event.extendedProps.link!='') {
    $('#event-content').append('<div><a href="' + event.extendedProps.link + '" target="_blank">'  + event.extendedProps.link + '</a></div>');
  }
  if(event.extendedProps.article_url) {
    $('#event-content').append('<div class="event-article">Vedi anche:<div>' + event.extendedProps.article_topic + ': <a href="' + event.extendedProps.article_url + '">'  + event.extendedProps.article_title + '</a></div></div>');
  }
  if(event.extendedProps.org_url) {
    $('#event-content').append('<div class="event-org">Organizzato da <a href="' + event.extendedProps.org_url + '">'  + event.extendedProps.org_name + '</a></div>');
  }
  $('#event-content').append('<div class="icon add-calendar">Aggiungi a calendario: <a href="' + event.extendedProps.export_google + '" target="_blank">Google</a> - <a href="' + event.extendedProps.export_webcal + '" target="_blank">Outlook</a> - <a href="' + event.extendedProps.export_webcal + '" target="_blank">Apple</a> - <a href="' + event.extendedProps.export + '" target="_blank">Altri</a></div>');
  $('#event-content').append('</div>');
  
  $('#event-content').modal();
  $('#event-content').on($.modal.CLOSE, function(event, modal) {
    document.title = 'Calendario di PeaceLink';
  });
}

/**
 * Mastodon User Timeline Widget
 * Adapted from https://github.com/AzetJP/mastodon-timeline-widget
 */

var MastodonApi = function(params_, endpoint) {

  // endpoint access settings
  this.INSTANCE_URI        = params_.instance_uri;
  this.ACCESS_TOKEN        = params_.access_token;
  this.ACCOUNT_ID          = params_.account_id;
  // optional parameters
  this.toots_limit         = params_.toots_limit || 20;
  this.picIcon             = params_.pic_icon || '[PICTURE]';
  this.boostsCountIcon     = params_.boosts_count_icon || '[Boost]';
  this.favouritesCountIcon = params_.favourites_count_icon || '[Favourite]';

  // display target element
  this.widget = $(params_.target_selector);

  // build the basic widget
  this.makeWidget();
  this.listStatuses(endpoint);

  // nsfw toggle
  // jQuery event handler
  var toggleNsfwMedia = function(e_) {
    e_.preventDefault();

    if($(this).hasClass('nsfw-opened')) {
      // hide image ===
      $(this).css({
        'background' : 'black'
      })
      .text('Clicca per mostrare')
      .removeClass('nsfw-opened');
    }
    else {
      // display image ===
      var img = $(this).attr('data-picpreview-url');
      $(this).css({
        'background'       : 'url('+img+') center center no-repeat'
        ,'background-size' : 'cover'
      })
      .text('')
      .addClass('nsfw-opened');
    }

  }


  /**
  * toggle the display of pictures in a modal-ish fashion
  *
  * @author Azet
  * @param jquery_event e_
  */
  // hidden media display toggle
  this.widget.on('click', '.toot-media-nsfw', toggleNsfwMedia);

}
/* <<< end constructor */


/**
* build timeline widget
*/
MastodonApi.prototype.makeWidget = function() {
  this.widget.addClass('mastodon-timeline');
  this.widget.append($('<div class="mt-header"><div class="user-link"></div></div>'));
  this.widget.append($('<div class="mt-body"><div class="mt-loading">loading...</div></div>'));
  this.widget.append($('<div class="mt-footer"></div>'));
};


/**
* listing function
*/
MastodonApi.prototype.listStatuses = function(mastodonUrl) {
  var mapi = this;
  $.ajax({
    url: mastodonUrl
    ,method : 'GET'
    ,dataType: 'json'
    ,success: function(data_) {
      $('.mt-body', mapi.widget).html("");
        for(var i in data_) {
          if(i==0) {
            var account = data_[i].account;
            setHeaderUserLink.call(mapi, account);
            setFooterLink.call(mapi, account);
          }
          if(data_[i].visibility=='public') {
            appendStatus.call(mapi, data_[i]);
          }
        }
        $('a', mapi.widget).attr('target', '_blank');
      }
      ,error: function(d_) {
        if(d_.responseJSON) {
          $('.mt-header', mapi.widget).html('ERROR');
          $('.mt-body', mapi.widget).html( '<div class="mt-error">' + d_.responseJSON.error + '</div>');
        }
      }
  });


  /**
  * add user link
  * @param object account_
  */
  var setHeaderUserLink = function(account_) {
    $('.user-link', this.widget).append("<a href='"+account_.url+"'>"+account_.display_name+"<span>@" + account_.username + "@sociale.network</span></a>");
    $('.user-link', this.widget).css({'background' : "white url('" + account_.avatar_static + "') 0 50% no-repeat",'background-size' :'contain'});
  };


  /**
  * add user link
  * @param object account_
  */
  var setFooterLink = function(account_) {
    $('.mt-footer', this.widget).append("<a href='"+account_.url+"'>Registrati su sociale.network</a>");
  };


  /**
  * inner function to add each message in container
  * @param object status_
  */
  var appendStatus = function(status_) {
    var content;
    var date, url, avatar, user;

    content = $("<div class='toot-text'>" + status_.content + "</div>" + "<div class='toot-medias'></div>");

    var toot_title = "<div class='mt-post'>"

    if(status_.reblog) {
      date = prepareDateDisplay(status_.reblog.created_at);
      url = status_.reblog.url;
      toot_title += "<a href='"+status_.reblog.account.url+"'>@"+status_.reblog.account.username+"</a>";
    } else {
      date = prepareDateDisplay(status_.created_at);
      url = status_.url;
      toot_title += "<a href='"+status_.account.url+"'>@"+status_.account.username+"</a>";
    }

    // format date
    var timestamp = "<a href='"+url+"'>" + date + "</a>";

    toot_title += ' - ' + timestamp + '</div>'

    var toot = $("<div class='mt-toot'></div>");

    toot.append( toot_title );
    toot.append( content );
    $('.mt-body', this.widget).append(toot);

    // media attachments? >>>
    if(status_.media_attachments.length>0) {
      var pic;
      for(var picid in status_.media_attachments) {
        pic = this.replaceMedias(content, status_.media_attachments[picid], status_.sensitive);
        toot.append( '<a href="'+url+'">' + pic + '</a>');
      }
    } else if(status_.card && status_.card.image) {
      toot.append( '<a href="'+url+'"><div class="toot-media-preview" style="background-image:url('+status_.card.image+');"></div></a>' );
    } else if(status_.reblog && status_.reblog.card) {
      toot.append( '<a href="'+url+'"><div class="toot-media-preview" style="background-image:url('+status_.reblog.card.image+');"></div></a>');
    }
  };

  /**
  * display toot time
  *
  * @author Azet
  * @param StringDate date_ (standard time format)
  * @return String
  */
  var prepareDateDisplay = function(date_) {
    var displayTime = "";
    var date = new Date( date_ );

    displayTime = date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+" "+date.getHours()+":"+("0"+date.getMinutes()).replace(/0(\d{2})/, "$1");

    return displayTime;
  };
};


/**
* replace media (pictures) in text with an icon and appends a preview
*
* @author Azet
* @param jquery_object content
* @param object media_ (received with toot's JSON data)
* @param bool nsfw_ indicates the media is not to be displayed
* @return object modifier content object
*/
MastodonApi.prototype.replaceMedias = function(content, media_, nsfw_) {
  var nsfw = nsfw_ || false;

  // icon in place of link in content
  var icon = '<a href="'+media_.url+'" class="toot-media-link" target="_blank">'+this.picIcon+'</a>';
  $('a[href="'+media_.text_url+'"]', content).replaceWith(icon);

  if(nsfw) {
    // pics hidden
    var pic = '<div class="toot-media-preview toot-media-nsfw" style="background:black;" data-picpreview-url="'+media_.preview_url+'">Clicca per mostrare</div>';
  } else {
    // pics visible
    var pic = '<div class="toot-media-preview" style="background-image:url('+media_.preview_url+');"></div>';
  }

  return pic;
};
