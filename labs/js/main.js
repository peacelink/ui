var dateObj = new Date();
var firstJan = new Date(dateObj.getFullYear(), "0", "1");
var secondsUpToNow = dateObj.getTime() / 1000 - firstJan.getTime() / 1000;
var secondsYear = 365 * 24 * 3600;
var counter = null;

$(function() {
  
  $('#counter1').click(function() {
    pckCounterClear();
    pckCounter1(747000000,0,9,'euro spesi dall\'inizio di quest\'anno per i cacciabombardieri F35','https://www.peacelink.it/disarmo/a/46129.html');
  });

  $('#counter2').click(function() {
    pckCounterClear();
    pckCounter2(85000/3,'bambini sotto i 5 anni morti di fame in Yemen dall\'inizio dell\'anno per colpa della guerra, combattuta anche con armi italiane. Uno ogni 18 minuti.','https://www.theguardian.com/world/2018/nov/21/yemen-young-children-dead-starvation-disease-save-the-children');
  });
  
  $('#counter3').click(function() {
    pckCounterClear();
    pckCounter3(2500000,'bambini sotto i 5 anni morti per malnutrizione dall\'inizio di quest\'anno nel mondo.','https://www.savethechildren.it/cosa-facciamo/pubblicazioni/lontani-dagli-occhi-lontani-dai-cuori');
  });

  $('#counter4').click(function() {
    pckCounterClear();
    pckCounter4(2500000,'bambini sotto i 5 anni morti per malnutrizione dall\'inizio di quest\'anno nel mondo.<br/>Cinque ogni minuto.','https://www.savethechildren.it/cosa-facciamo/pubblicazioni/lontani-dagli-occhi-lontani-dai-cuori');
  });
  
  $('#counter5').click(function() {
    pckCounterClear();
    pckCounter5(85000/3,'bambini sotto i 5 anni morti di fame in Yemen dall\'inizio dell\'anno per colpa della guerra. Uno ogni 18 minuti. ',275000000,'armi italiane all\'Arabia Saudita dall\'inizio di quest\'anno','https://www.theguardian.com/world/2018/nov/21/yemen-young-children-dead-starvation-disease-save-the-children');
  });
  
  if(location.hash.length) {
    var counterId = document.URL.substring(document.URL.indexOf('#')+1)
    if(counterId.length) {
      $('#counter'+counterId).trigger('click');
    }
  }

});

function pckCounterClear() {
  $('#counters').addClass('hidden');
  $('#pck-counter').html('');
  $('#pck-counter').removeClass('banner');
  $('#pck-counter').removeClass('progressbar');
  $('#pck-counter').removeClass('progresscircle');
  $('#pck-counter').removeClass('progresscircle-small');
  if(counter) {
    counter.stop();
  }
}
function pckCounterReset() {
  $('#pck-counter').html('');
}

function pckCounter1(numberPerYear,decimals,maxDigits,text,link) {
  $('#pck-counter').addClass('banner');
  var odometer = $('<div/>');
  odometer.addClass('odometer-container');
  odometer.append($('<div class="odometer"/>'));
  $('#pck-counter').append(odometer)
  if($('#pck-counter .odometer').length) {
    var numberNow = (numberPerYear * secondsUpToNow / secondsYear).toFixed(decimals);
    var increment = numberPerYear / secondsUpToNow;
    $('.odometer').jOdometer({
      increment: increment, 
      counterStart:numberNow, 
      delayTime: 1000,
      speed: 1000,
      counterEnd:false, 
      formatNumber:true, 
      numbersImage: '/js/images/jodometer-numbers-color.png',
      maxDigits: maxDigits,
      spaceNumbers: 0
    });
  $('#pck-counter').append($('<div class="counter-info"/>').html('<a href="'+link+'">'+text+'</a>'))
  }
}

function pckCounter2(numberPerYear,text,link) {
  var numberNow = (numberPerYear * secondsUpToNow / secondsYear);
  var numberNowDisplay = numberNow | 0;
  var numberNowPartial = numberNow % 1;
  var seconds = secondsYear / (numberPerYear);
  $('#pck-counter').addClass('progressbar');
  pckCounter2Recurse(numberNowDisplay,numberNowPartial,seconds,text,link);
}

function pckCounter2Recurse(numberNowDisplay,numberNowPartial,seconds,text,link) {
  pckCounterReset();
  counter = new ProgressBar.Line('#pck-counter', {
      color: '#000000',
      easing: 'linear',
      strokeWidth: 40,
      duration: 1000,
      svgStyle: {width: '100%', height: '100%'},
      text: {
        value: '<a href="'+link+'"><div class="value">'+numberNowDisplay+'</div><div class="description"> '+text+'</div><div class="timer"></div></a>',
        className: 'counter-info',
        style: null
      }
  });
  var animationLength = seconds
  var secondsLeft = seconds
  if(numberNowPartial>0) {
    counter.set(numberNowPartial);
    animationLength = seconds * (1-numberNowPartial)
  }
  counter.animate(1, {
    duration: animationLength * 1000, //ms
    step: function(state) {
      secondsLeft = ((state.offset * seconds / 100) | 0) + 1;
      minutesLeft = Math.floor(secondsLeft/60);
      secondsLeft -= minutesLeft * 60;      
      $('#pck-counter .timer').html(minutesLeft+':'+(secondsLeft < 10 ? '0'+secondsLeft : secondsLeft))
    }
  }, function() {
    // increment value by 1
    numberNowDisplay += 1;
    pckCounter2Recurse(numberNowDisplay,0,seconds,text,link);
  });
}

function pckCounter3(numberPerYear,text,link) {
  var numberNow = (numberPerYear * secondsUpToNow / secondsYear);
  var numberNowDisplay = numberNow | 0;
  var numberNowPartial = numberNow % 1;
  var seconds = secondsYear / (numberPerYear);
  $('#pck-counter').addClass('progresscircle-small');
  pckCounter4Recurse(numberNowDisplay,numberNowPartial,seconds,text,link,50);
}
function pckCounter4(numberPerYear,text,link) {
  var numberNow = (numberPerYear * secondsUpToNow / secondsYear);
  var numberNowDisplay = numberNow | 0;
  var numberNowPartial = numberNow % 1;
  var seconds = secondsYear / (numberPerYear);
  $('#pck-counter').addClass('progresscircle');
  pckCounter4Recurse(numberNowDisplay,numberNowPartial,seconds,text,link,10);
}
function pckCounter4Recurse(numberNowDisplay,numberNowPartial,seconds,text,link,strokeWidth) {
  pckCounterReset();
  counter = new ProgressBar.Circle('#pck-counter', {
      color: '#000000',
      trailColor: '#888888',
      easing: 'linear',
      strokeWidth: strokeWidth,
      duration: 1000,
      svgStyle: {width: '100%', height: '100%'},
      text: {
        value: '<a href="'+link+'"><div class="value">'+numberNowDisplay+'</div><div class="description"> '+text+'</div></a>',
        className: 'counter-info',
        style: null
      }
  });
  var animationLength = seconds
  var secondsLeft = seconds
  if(numberNowPartial>0) {
    counter.set(numberNowPartial);
    animationLength = seconds * (1-numberNowPartial)
  }
  counter.animate(1, {
    duration: animationLength * 1000 //ms
  }, function() {
    // increment value by 1
    numberNowDisplay += 1;
    pckCounter4Recurse(numberNowDisplay,0,seconds,text,link,strokeWidth);
  });
}

function pckCounter5(number1PerYear,text1,number2PerYear,text2,link) {
  var number1Now = (number1PerYear * secondsUpToNow / secondsYear);
  var number1NowDisplay = number1Now | 0;
  var number1NowPartial = number1Now % 1;
  var seconds = secondsYear / (number1PerYear);
  $('#pck-counter').addClass('progressbar2');
  pckCounter5Recurse(number1NowDisplay,text1,number1NowPartial,number2PerYear,text2,seconds,link);
}

function pckCounter5Recurse(number1NowDisplay,text1,number1NowPartial,number2PerYear,text2,seconds,link) {
  var secondsUpToNow = dateObj.getTime() / 1000 - firstJan.getTime() / 1000;
  var number2Now = (number2PerYear * secondsUpToNow / secondsYear);
  pckCounterReset();
  counter = new ProgressBar.Line('#pck-counter', {
      color: '#000000',
      easing: 'linear',
      strokeWidth: 40,
      duration: 1000,
      svgStyle: {width: '100%', height: '100%'},
      text: {
        value: '<a href="'+link+'"><div class="value" id="value1">'+number1NowDisplay.toLocaleString('it')+'</div><div class="description" id="desc1"> '+text1+'<span class="timer2"></span></div><div class="value" id="value2">'+number2Now+'</div><div class="description" id="desc2"> '+text2+'</div></a>',
        className: 'counter-info',
        style: null
      }
  });
  var animationLength = seconds
  var secondsLeft = seconds
  if(number1NowPartial>0) {
    counter.set(number1NowPartial);
    animationLength = seconds * (1-number1NowPartial)
  }
  counter.animate(1, {
    duration: animationLength * 1000, //ms
    step: function(state) {
      var date2Obj = new Date();
      secondsUpToNow = date2Obj.getTime() / 1000 - firstJan.getTime() / 1000;
      number2Now = ((number2PerYear * secondsUpToNow / secondsYear)).toLocaleString('it-IT',{ style: 'currency', currency: 'EUR' });
      $('#pck-counter #value2').html(number2Now)
      secondsLeft = ((state.offset * seconds / 100) | 0) + 1;
      minutesLeft = Math.floor(secondsLeft/60);
      secondsLeft -= minutesLeft * 60;      
      $('#pck-counter .timer2').html('Il prossimo tra '+ minutesLeft+':'+(secondsLeft < 10 ? '0'+secondsLeft : secondsLeft))
    }
  }, function() {
    // increment value by 1
    number1NowDisplay += 1;
    pckCounter5Recurse(number1NowDisplay,text1,0,number2PerYear,text2,seconds,link);
  });
}


hs.graphicsDir = '/js/highslide/graphics/';
