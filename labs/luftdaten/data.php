<?php
/**
 * Get json payload from Lufdaten sensor
 * and write to thingspeak.com channel
 *
 */
 
$json = file_get_contents('php://input');
$data = json_decode($json,false);
$debug = $json;

$ts_api_key = getenv('TS_API_KEY');
$esp8266id = getenv('ESP8266ID');

if(isset($data->esp8266id) && $data->esp8266id == $esp8266id) {
  $url = "https://api.thingspeak.com/update?api_key=$ts_api_key";
  $values = array();
  foreach($data->sensordatavalues as $value) {
    switch($value->value_type) {
      case 'SDS_P1':
        $pm10 = $value->value;
        $url .= "&field1={$value->value}";
      break;
      case 'SDS_P2':
        $pm25 = $value->value;
        $url .= "&field2={$value->value}";
      break;
      case 'temperature':
        $url .= "&field3={$value->value}";
      break;
      case 'humidity':
        $humidity = $value->value;
        $url .= "&field4={$value->value}";
      break;
    }
  }
  if(isset($pm10) && isset($pm25)) {
    $pm10b = $humidity>0? $pm10 / (1.0 + 0.81559 * pow(($humidity / 100.0), 5.83411)) : $pm10;
    $pm25b = $humidity>0? $pm25 / (1.0 + 0.48756 * pow(($humidity / 100.0), 8.60068)) : $pm25;
    $url .= "&field5=$pm10b&field6=$pm25b";
  }
  $debug = $url;

  $curl_handle = curl_init();
  curl_setopt( $curl_handle, CURLOPT_URL, $url );
  curl_setopt( $curl_handle, CURLOPT_RETURNTRANSFER, true);
  curl_setopt( $curl_handle, CURLOPT_FOLLOWLOCATION, 0);
  curl_exec( $curl_handle );
  curl_close( $curl_handle );
}

/*
$fp = fopen('debug.log', 'a');
fwrite($fp, $debug);
fclose($fp);
*/
?>
