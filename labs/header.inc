<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PeaceLabs</title>
    <link type="text/css" rel="stylesheet" href="/css/reset.css" media="screen"/>
    <link type="text/css" rel="stylesheet" href="/css/pcklabs.css" media="screen"/>
    <script type="text/javascript" src="/js/lib.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
  </head>
  <body>
    <a id="logo" href="/"><img src="/peacelabs.jpg" width="300"/></a>
