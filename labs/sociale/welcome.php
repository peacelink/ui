<?php
/**
 * Get json status from Mastodon server
 *
 */

include('common.php');

$cache = "../../cdn/cache/last_follower_id.txt";

$last_user_id = "10719";

$ip = $_SERVER['REMOTE_ADDR'];
if($ip!='54.171.181.154') {
    header("HTTP/1.1 401 Unauthorized");
    die("access denied");
}

$data = mastorequest($tokens['koala'],"accounts/{$ids['koala']}/followers");

$json = json_decode($data, TRUE);
$newest_user_id=$json[0]['id'];

if(file_exists($cache)) {
    $readstorage = fopen($cache, "r");
    $last_user_id = fread($readstorage,filesize($cache));
}

// check if we have new users
if (($last_user_id < $newest_user_id) && ($last_user_id != "") && ($newest_user_id != "")) {

    $writestorage = fopen($cache, "w");
    fwrite($writestorage, $newest_user_id);
    fclose($writestorage);
    
    // Welcome message
    $readstorage = fopen('welcome.txt', "r");
    $welcome_message = fread($readstorage,filesize('welcome.txt'));

    // Loop the user list
    foreach( array_reverse($json) as $user_json ) {
        $user_id = $user_json['id'];
        $user_username = $user_json['username'];

        // pick only newest users
        if (($last_user_id < $user_id) && ($user_id != "")) {

            // Post Mastodon message through Mastodon API
            $display_name = $user_json['display_name']!=""? $user_json['display_name'] : $user_username;
            
            $status_data = array(
                "status" => "@" . $user_username . " " . sprintf($welcome_message,$display_name) ,
                "language" => "it",
                "visibility" => "direct"
            );
            mastopost($tokens['koala'], "statuses", $status_data);

            echo "Welcome new user: ".$user_id.": @".$user_username."\n";
        }
    }
} else {
    echo "\nNo new user\n";
}

?>