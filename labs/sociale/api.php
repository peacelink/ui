<?php
/**
 * Get json status from Mastodon server
 *
 */

include('common.php');

$cache = "$cache_dir/sociale.json";

$ttl = 5;

if(! (file_exists($cache) && (time() - filemtime($cache) < ($ttl * 60) ))) {
  $data = mastorequest('',"accounts/{$ids['pck']}/statuses");
  $fp = fopen($cache, 'w');
  flock($fp,LOCK_EX);
  fwrite($fp, $data);
  flock($fp,LOCK_UN);
  fclose($fp);
}

$content = '';
$fp = @fopen ($cache, "r");
if ($fp) {
  while (!feof ($fp)) {
    $content .= fgets($fp, 1024);
  }
}
fclose($fp);

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
echo $content;
?>
