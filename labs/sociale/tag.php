<?php
/**
 * Get json tag from Mastodon server
 *
 */

include('common.php');

$tag = $_GET['tag'];

$cache = "$cache_dir/tag_{$tag}.json";

$ttl = 5;

if(true || ! (file_exists($cache) && (time() - filemtime($cache) < ($ttl * 60) ))) {
  $data = mastorequest('',"timelines/tag/{$tag}");
  $fp = fopen($cache, 'w');
  flock($fp,LOCK_EX);
  fwrite($fp, $data);
  flock($fp,LOCK_UN);
  fclose($fp);
}

$content = '';
$fp = @fopen ($cache, "r");
if ($fp) {
  while (!feof ($fp)) {
    $content .= fgets($fp, 1024);
  }
}
fclose($fp);

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
echo $content;
?>
