<?php

$tokens = array(
    'pck' => getenv('PCK_TOKEN'),
    'koala' => getenv('KOALA_TOKEN')
);

$ids = array(
    'pck' => "6990",
    'koala' => "10719"
);

$server = 'https://sociale.network';

$cache_dir = "../../cdn/cache";

function mastorequest($token, $endpoint) {
    global $server;
    $url = "{$server}/api/v1/{$endpoint}";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    if($token!='') {
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
        ));
    }
    $data = curl_exec($ch);
    // $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $data;
}

function mastopost($token, $endpoint, $post) {
    global $server;
    $url = "{$server}/api/v1/{$endpoint}";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    if($token!='') {
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $token
        ));
    }
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}
?>
