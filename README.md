# PeaceLink User Interface

La grafica del sito di PeaceLink non e' piu' gestita dall'interno di PhPeace, ma in questo repository.

Esistono due branch: `dev` e `master`

L'attivita' di sviluppo di svolge in `dev`, o in sue branch derivate.

Ogni aggiornamento a `dev` viene automaticamente pubblicato su https://test.peacelink.it, e notificato in PeaceChat.

Per pubblicare sul sito live, occorre aprire una PR da `dev` a `master`, fare il merge e collegarsi via SSH per il deployment manuale (`pckui_deploy.sh live`)


## Test

https://test.peacelink.it contiene una copia dei dati del sito, con autori e utenti anonimizzati e varie funzioni (mailing, notifiche) disabilitate.

Serve solo a verificare gli aggiornamenti della grafica. Per gli utenti di PhPeace con indirizzo @peacelink e' comunque possibile entrare nella sua interfaccia di amministrazione https://admin.test.peacelink.it per sperimentare con i contenuti e la grafica.

Il database di test viene periodicamente sincronizzato manualmente con apposito script.

## CSS

Esiste un solo file CSS per tutto il sito di PeaceLink: [pck.css](css/pck.css)

Il CSS e' diviso in 5 breaking points

- fino a 599px per mobile
- da 600px a 800px per mobile landscape
- da 801px a 1024px per tablet
- da 1025px a 1124 per desktop
- oltre 1125px per schermi larghi

C'e' anche una sezione per la stampa.

I vecchi fogli di stile sono stati preservati solo per gli ospiti.

## XSL

Il sito di PeaceLink ora e' in fork totale con PhPeace per quanto riguarda i template XSL. Ovvero i template distribuiti e aggiornati da PhPeace non vengono usati.

Il livello [0](xsl/0) viene usato per PeaceLink, mentre gli ospiti non vengono piu' aggiornati e puntano a [old](xsl/0), una copia del livello 0 prima del fork 


# Impostazione grafica

## Navigazione

Esistono due barre di navigazione, una dedicata a PeaceLink e una ai contenuti.

Quella di PeaceLink punta a un massimo di 5 sottosezioni (c.d. argomenti) della tematica di PeaceLink.

La navigazione dei contenuti punta a 5 aree tematiche, che sono state definite in bas allo statuto di PeaceLink e che accorpano tutte le tematiche del sito: 

- Pace
- Cultura
- Solidarietà
- Cittadinanza attiva
- Ecologia

Seguono link agli ospiti, al calendario e alla pagina di ricerca

Le aree tematiche corrispondono a 5 gruppi di tematiche in PhPeace: https://admin.peacelink.it/topics/tree.php

## Homepage

L'homepage e' organizzata in 6 sezioni, corrispondenti alle 5 aree tematiche piu' una dedicata a PeaceLink.

Le sezioni sono ordinate in ordine cronologico, ovvero in base all'articolo pubblicato piu' di recente.

All'interno di ogni sezione sono sempre presenti 4 articoli, non piu' di 2 per tematica.

E' stato introdotto un apposito flag nel form di inserimento articolo per metterli in evidenza in homepage. Questo flag puo' essere impostato solo dagli amministratori del modulo tematiche, non dagli amministratori di tematica.

Se un articolo e' messo in evidenza, appare in risalto all'interno della sua sezione, e i rimanenti articoli vengono mostrati su 3 colonne senza immagini.

E' possibile mettere in evidenza un solo articolo per ogni sezione. Se piu' articoli hanno il flag impostato, vince quello piu' recente.

La sezione di PeaceLink ha l'ultimo editoriale sempre in evidenza, e di conseguenza gli altri articoli (con il massimo di 1 per ogni tematica del gruppo PeaceLink) su 3 colonne.

La sezione Cultura, vista la carenza di immagini, viene mostrata su due colonne escludendo al momento eventuali immagini associate agli articoli.

## Spalla destra

La spalla destra e' sempre presente, anche se viene linerizzata su mobile. Contiene contenuti dinamici, sempre aggiornati.

- l'ultima fotonotizia
- gli ultimi tweet
- gli ultimi post su Facebook
- i prossimi eventi
- la lista news (solo nella pagine globali)

## Spalla sinistra

La spalla sinistra appare solo nelle tematiche, e contiene 

- l'abero di navigzione
- l'eventuale mailing-list associata
- gli ultimi articoli dal sito

