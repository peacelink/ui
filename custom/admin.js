// Additional validation checks

$(function() {

  var maxHalftitleLength = 100;
  var maxHeadlineLength = 100;
  var maxSummaryLength = 300;
  var maxWordLength = 21;
  
  if($('#content.mod-articles #article-form').length) {
    checkFieldLimit('subhead',maxSummaryLength,'Il sommario');
    checkFieldLimit('halftitle',maxHalftitleLength,'L\'occhiello');
    checkFieldLimit('headline',maxHeadlineLength,'Il titolo');

    bindFieldLimit('subhead',maxSummaryLength,'Il sommario');
    bindFieldLimit('halftitle',maxHalftitleLength,'L\'occhiello');
    bindFieldLimit('headline',maxHeadlineLength,'Il titolo');
  }
  
  function checkFieldLimit(field,limit,name) {
    var urlRegex = new RegExp("(https?://)");

    var length = $('#content.mod-articles #article-form textarea#'+field+'-field').val().length;
    $('#content.mod-articles #article-form tr#finput-'+field+' .content-warning').remove();
    $('#content.mod-articles #article-form tr#finput-'+field).css('background-color','transparent');
    var warnings = [];
    if(length > limit) {
      $('#content.mod-articles #article-form tr#finput-'+field).css('background-color','#fb9a9a');
      warnings.push(name+' è troppo lungo!');
    }
    value = $('#content.mod-articles #article-form textarea#'+field+'-field').val();
    if(urlRegex.test(value)) {
      warnings.push(name+' contiene un link!');
    }
    if(value.length>3 && value === value.toUpperCase()) {
      warnings.push(name+' è in maiuscolo!');
    }
    // split by space and get every words length
    var words = $('#content.mod-articles #article-form textarea#'+field+'-field').val().split(/\s+/);
    var maxWord = Math.max.apply(Math, $.map(words, function (el) { return el.length }));
    if(maxWord > maxWordLength) {
      warnings.push(name+' contiene una parola troppo lunga! Si prega di dividerla con degli spazi, o inserirla nel testo');
    }
    if(warnings.length) {
      $('#content.mod-articles #article-form tr#finput-'+field).css('background-color','#fb9a9a');
      $('#content.mod-articles #article-form textarea#'+field+'-field').after('<div class="content-warning">'+warnings.join('<br>')+'</div>');
    }
  }
  
  function bindFieldLimit(field,limit,name) {
    $('#content.mod-articles #article-form textarea#'+field+'-field').bind('input', function() {
      checkFieldLimit(field,limit,name)
    })
  }
  
})
