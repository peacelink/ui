<?php
$allowed_paths = [
		'banners',
		'covers',
		'events',
		'graphics',
		'images',
		'orgs',
		'products',
		'qrcodes',
		'users',
		'videos'
];

$allowed_widths = [
		'80',
		'200',
		'300',
		'400',
		'500'
];

$max_width = 2048;

if (isset($_GET['id']) && isset($_GET['ext']) && isset($_GET['path']) && in_array($_GET['path'], $allowed_paths)) {

	$id_image = $_GET['id'];
	$ext = $_GET['ext'];
	$path = $_GET['path'];

	$allowed_formats = [
			'jpg',
			'png'
	];

	$format = isset($_GET['format']) && in_array($_GET['format'], $allowed_formats)? $_GET['format'] : 'jpg';

	if (isset($_GET['w']) && is_numeric($_GET['w'])) {
		$width = $_GET['w'];
		$mode = $width;
		
		if($width > $max_width) {
			http_response_code(403);
			exit;
		}
	} else {
		$mode = 'orig';
	}

	switch ($path) {
		case 'graphics' :
			$filename = "../graphics/orig/{$id_image}.{$ext}";
		break;
		case 'qrcodes' :
			$filename = "../uploads/qrcodes/{$id_image}.{$ext}";
		break;
		case 'videos' :
			$filename = "../uploads/videos/thumbs/{$id_image}.{$ext}";
		break;
		default :
			$filename = "../uploads/{$path}/orig/{$id_image}.{$ext}";
	}

	if (! is_null($filename) && file_exists($filename)) {

		if ($path=='qrcodes') {
			switch ($ext) {
				case 'svg' :
					header("Content-Type: image/svg+xml");
					header('Content-Length: ' . filesize($filename));
					readfile($filename);
				break;
				case 'png' :
					header("Content-Type: image/png");
					header('Content-Length: ' . filesize($filename));
					readfile($filename);
				break;
				default :
					http_response_code(404);
			}
		} else {
			$file_info = getimagesize($filename);

			if (isset($file_info['0']) && isset($file_info['1'])) {
				if ($mode == 'orig') {
					$width = $file_info['0'];
					$height = $file_info['1'];
				} else {
					$height = $file_info['1'] * $width / $file_info['0'];
				}
				$outfile = "cache/{$path}/{$mode}/{$id_image}.{$format}";
				$save_dir = dirname($outfile);
				if (! file_exists($save_dir)) {
					mkdir($save_dir, 0777, true);
				}
	
				$image = new Imagick($filename);
				if ($mode != 'orig') {
					$image->resizeImage($width, $height, imagick::FILTER_LANCZOS, 1);
				}
	
				$image->setImageFormat($format);
				switch ($format) {
					case 'jpg' :
						$image->setImageCompression(Imagick::COMPRESSION_JPEG);
						$image->setImageCompressionQuality(90);
						$mime_type = 'image/jpeg';
					break;
					case 'png' :
						$image->setCompressionQuality(5);
						$mime_type = 'image/png';
					break;
				}
				$image->writeImage($outfile);
	
				if (file_exists($outfile)) {
					header("Content-Type: {$mime_type}");
					header('Content-Length: ' . filesize($outfile));
					header("X-Source: PhPeace JIT image resize");
					readfile($outfile);
					/*
					 * $fp = fopen($outfile, "rb");
					 * if ($fp) {
					 * fpassthru($fp);
					 * }
					 */
				} else {
					// Conversion error
					http_response_code(424);
				}
			} else {
				// File error
				http_response_code(422);
			}
		}
	} else {
		// 404
		http_response_code(404);
	}
}
?>