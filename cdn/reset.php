<?php
$allowed_paths = [
    'banners',
    'covers',
    'events',
    'graphics',
    'images',
    'orgs',
    'products',
    'users',
    'videos'
];
$allowed_formats = [
    'jpg',
    'png'
];

if (isset($_GET['id']) && is_numeric($_GET['id']) && isset($_GET['ext']) && in_array($_GET['ext'],$allowed_formats) && isset($_GET['path']) && in_array($_GET['path'], $allowed_paths)) {
    // auth
    $id = (int)$_GET['id'];
    if($id>0 && isset($_GET['token']) && $_GET['token']!='' && $_GET['token']==getenv('CDN_TOKEN')) {
        // for each dir
        $directory = "cache/{$_GET['path']}";
        $handle = opendir($directory);
        while(false !== ($path = readdir($handle))) {
            if ($path != "." && $path != ".." && $path != "orig" && is_dir("$directory/$path")) {
                $filename = "$directory/$path/{$id}.{$_GET['ext']}";
                // delete
                if(file_exists($filename)) {
                    unlink($filename);
                }
            }
        }
        if(isset($_GET['orig']) && $_GET['orig']=='1') {
            $filename = "$directory/orig/{$id}.{$_GET['ext']}";
            if(file_exists($filename)) {
                unlink($filename);
            }
        }
    }
}
?>